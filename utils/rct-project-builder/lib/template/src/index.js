import React from 'react'
import { render } from 'react-dom'
import PropTypes from 'prop-types'
import { useStrict } from 'mobx'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'mobx-react'
// load roboto font to entrypoint
// eslint-disable-next-line import/extensions
import 'typeface-roboto'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import blue from 'material-ui/colors/blue'
import pink from 'material-ui/colors/pink'

import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: pink,
  },
})

const stores = {
}

class ErrorBoundary extends React.Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  // eslint-disable-next-line no-unused-vars
  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true })
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, info)
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <h1>Something went wrong.</h1>
      )
    }
    return this.props.children
  }
}

// For easier debugging
// eslint-disable-next-line no-underscore-dangle
window._____APP_STATE_____ = stores

useStrict(true)

render(
  <Provider {...stores}>
    <BrowserRouter>
      <MuiThemeProvider theme={theme}>
        <ErrorBoundary>
          <App />
        </ErrorBoundary>
      </MuiThemeProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
)

registerServiceWorker()
