import React, { Component } from 'react'
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { Switch, Route, withRouter, Link } from 'react-router-dom'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { Redirect } from 'react-router'

function PrivateRoute({ component: RouteComponent, authed, ...rest }) {
  if (authed) {
    return <Route {...rest} render={props => <RouteComponent {...props} />} />
  }
  return (
    <Route
      {...rest}
      render={props => (
        <Redirect
          to={{
            pathname: '/login',
            state: {
              // eslint-disable-next-line react/prop-types
              from: props.location,
            },
          }}
        />
      )}
    />
  )
}
PrivateRoute.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  component: PropTypes.any.isRequired,
  authed: PropTypes.bool.isRequired,
}

// @inject('swaggerClientStore', 'commonStore')
@withRouter
@observer
class App extends Component {
  static propTypes = {
    // commonStore: PropTypes.instanceOf(RctCommonStore).isRequired,
    // swaggerClientStore: PropTypes.instanceOf(RctSwaggerClientStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    location: PropTypes.object.isRequired,
  }

  // async componentWillMount() {
  //   await this.props.swaggerClientStore.buildClient()
  //   this.props.commonStore.setAppLoaded()
  // }

  render() {
    // const { isLoggedIn, appLoaded } = this.props.commonStore
    // if (!appLoaded) {
    //   return (<div>Loading...</div>)
    // }
    return (
      <div>
        {/*
        <Switch>
          <Route path="/aaa" component={AaaComponent} />
          <Route path="/bbb" component={BbbComponent} />
          <Route path="/ccc" component={CccComponent} />
          <PrivateRoute authed={isLoggedIn} path="/ddd" component={DddComponent} />
          <Route path="/" component={HomeComponent} />
        </Switch>
        */}
      </div>
    )
  }
}

export default App
