const debug = require('debug')('react-packages:utils:rct-project-builder:lib')
const Mustache = require('mustache')
const fs = require('fs')
const path = require('path')
const gitP = require('simple-git/promise')
const _ = require('lodash')
const yaml = require('js-yaml')
const Swagger = require('swagger-client')

const ROOT_FOLDER = path.resolve(process.env.HOME, './Dev')
debug(ROOT_FOLDER)

async function createDirectories({ group, name, directories }) {
  debug('createDirectories()')
  directories.forEach((dir) => {
    const dirPath = `${ROOT_FOLDER}/${group}/${name}/${dir}`
    debug(dirPath)
    if (!fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath)
      debug(`the new folder ${dir} has been created!`)
    } else {
      debug(`the folder ${dir} already exists!`)
    }
  })
}
async function createFiles({
  group, name, variables, files,
}) {
  debug('createFiles()')
  files.forEach(({ src, dst }) => {
    debug(src)
    debug(dst)
    const tpl = fs.readFileSync(path.resolve(__dirname, `./template/${src}`), 'utf8')
    const output = Mustache.render(tpl, variables)
    debug(output)
    const dstPath = `${ROOT_FOLDER}/${group}/${name}/${dst}`
    debug(dstPath)
    fs.writeFileSync(dstPath, output, 'utf8')
    debug(`the file ${dst} has been created!`)
  })
}
async function buildSwaggerClient({ token }) {
  debug('build_swagger_client()')
  const { apis: { default: { ...operations } } } = await new Swagger({
    spec: yaml.safeLoad(fs.readFileSync(path.resolve(__dirname, './swagger/gitlab.yml'), 'utf8')),
    authorizations: { token },
  })
  return operations
}
async function getNamespaceId({ group, getNamespaces }) {
  debug('getNamespaceId()')
  const { body: [{ id: groupNamespaceId }] } = await getNamespaces({ search: group })
  debug(groupNamespaceId)
  return groupNamespaceId
}
async function createRemoteProject({
  name, description, groupNamespaceId, createProject,
}) {
  debug('createRemoteProject()')
  await createProject({
    namespace_id: groupNamespaceId,
    visibility: 'private',
    path: name,
    description: `${description} ([${name}](http://${name}.ws3.4ge.it))`,
  })
}
async function switchRunnerOn({ group, name, enableRunner }) {
  debug('switchRunnerOn()')
  await enableRunner({ projectId: `${group}/${name}`, runner_id: process.env.RUNNER_ID })
}
async function prepareRepository({ group, name }) {
  debug('prepareRepository')
  // setup git to working directory
  const git = gitP(`${ROOT_FOLDER}/${group}/${name}`)
  // git init
  await git.init()
  // git add
  await git.add('.')
  // git commit
  await git.commit('feat(init) new repository')
  // add remote repository URL
  await git.addRemote('origin', `git@gitlab.com:${group}/${name}.git`)
  // git push
  await git.push(['-u', 'origin', 'master'])
}
async function usage({ group, name }) {
  /* eslint-disable no-console */
  console.log()
  console.log()
  console.log(`The new repository has been properly generated and is available at https://gitlab.com/${group}/${name}`)
  console.log()
  console.log('You can clone the repository with the following command:')
  console.log()
  console.log(`git clone git@gitlab.com:${group}/${name}.git`)
  console.log()
  console.log('Or just get access to the locally generated folder:')
  console.log()
  console.log(`cd ${ROOT_FOLDER}/${group}/${name}`)
  console.log()
  /* eslint-enable no-console */
}

module.exports = async ({
  group, name, description, authorName, authorEmail, dryRun,
}) => {
  try {
    // debug
    debug(`group: ${group}`)
    debug(`name: ${name}`)
    debug(`description: ${description}`)
    debug(`authorName: ${authorName}`)
    debug(`authorEmail: ${authorEmail}`)
    debug(`dryRun: ${dryRun}`)

    // define variables
    const variables = {
      group,
      name,
      description,
      authorName,
      authorEmail,
      _class: _.upperFirst(_.camelCase(name)),
      _function: _.camelCase(name),
    }
    // define files
    const files = [
      { src: '.env', dst: '.env' },
      { src: '.eslintignore', dst: '.eslintignore' },
      { src: '.eslintrc.yml', dst: '.eslintrc.yml' },
      { src: '.gitignore', dst: '.gitignore' },
      { src: '.gitlab-ci.yml', dst: '.gitlab-ci.yml' },
      { src: 'config-overrides.js', dst: 'config-overrides.js' },
      { src: 'LICENSE', dst: 'LICENSE' },
      { src: 'package.json', dst: 'package.json' },
      { src: 'README.md', dst: 'README.md' },
      { src: 'public/favicon.ico', dst: 'public/favicon.ico' },
      { src: 'public/index.html', dst: 'public/index.html' },
      { src: 'public/manifest.json', dst: 'public/manifest.json' },
      { src: 'src/App.css', dst: 'src/App.css' },
      { src: 'src/App.js', dst: 'src/App.js' },
      { src: 'src/index.css', dst: 'src/index.css' },
      { src: 'src/index.js', dst: 'src/index.js' },
      { src: 'src/registerServiceWorker.js', dst: 'src/registerServiceWorker.js' },
    ]
    // define directories
    const directories = [
      '',
      'src',
      'public',
    ]

    // create directories
    await createDirectories({ group, name, directories })
    // create files
    await createFiles({
      group, name, variables, files,
    })
    // build swagger client
    const { getNamespaces, createProject, enableRunner } = await buildSwaggerClient({
      token: process.env.GITLAB_PRIVATE_TOKEN,
    })
    // get namespace id
    const groupNamespaceId = await getNamespaceId({ group, getNamespaces })
    await createRemoteProject({
      name, description, groupNamespaceId, createProject,
    })
    // enable runner
    await switchRunnerOn({ group, name, enableRunner })
    await prepareRepository({ group, name })
    // print usage
    await usage({ group, name })
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
    process.exit(-1)
  }
}
