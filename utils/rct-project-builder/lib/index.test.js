/* eslint-disable no-console */
const Swagger = require('swagger-client')
const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')

async function buildSwaggerClient({ token }) {
  const { apis: { default: { ...operations } } } = await new Swagger({
    spec: yaml.safeLoad(fs.readFileSync(path.resolve(__dirname, './swagger/gitlab.yml'), 'utf8')),
    authorizations: { token },
  })
  return operations
}

test('build swagger client', async () => {
  const { getNamespaces, createProject, enableRunner } = await buildSwaggerClient({
    token: process.env.GITLAB_PRIVATE_TOKEN,
  })
  expect(getNamespaces).toBeDefined()
  expect(createProject).toBeDefined()
  expect(enableRunner).toBeDefined()
})

test('create dummy project', async () => {
  const { getNamespaces/*, createProject*/ } = await buildSwaggerClient({
    token: process.env.GITLAB_PRIVATE_TOKEN,
  })
  const { body: [{ id: groupNamespaceId }] } = await getNamespaces({ search: 'canercandan' })
  expect(groupNamespaceId).toEqual(3310)
  // try {
  //   const obj = await createProject({
  //     namespace_id: groupNamespaceId,
  //     visibility: 'private',
  //     path: 'ccc1234',
  //     description: 'fake description',
  //   })
  //   console.log(obj)
  // } catch (err) {
  //   console.error(err)
  // }
})
