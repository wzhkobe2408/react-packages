#!/usr/bin/env node

const debug = require('debug')('react-packages:utils:rct-project-builder:bin')
const program = require('commander')
const inquirer = require('inquirer')
const lib = require('../')
const { version } = require('../package')

async function start() {
  debug('start()')

  program
    .description('Build a new package on react-packages.')
    .usage('--group <GROUP> --name <NAME> --description "<DESCRIPTION>"')
    .option('-g, --group <group>', 'the group repository where it will be located on gitlab.com, the one used in `<GROUP>/<NAME>`', '')
    .option('-n, --name <name>', 'the name of the repository the one used in `<GROUP>/<NAME>`', '')
    .option('-d, --description <description>', 'the description of the project stored in the `package.json` and `README.md`', '')
    .option('-a, --authorName <author_name>', 'the name of the author stored in the `package.json` generated when creating a new project', '')
    .option('-e, --authorEmail <author_email>', 'the name of the author stored in the `package.json` generated when creating a new project', '')
    .option('-R, --dry-run', 'dry run mode')
    .version(version)
    .parse(process.argv)

  const {
    group: pGroup,
    name: pName,
    description: pDescription,
    authorName: pAuthorName,
    authorEmail: pAuthorEmail,
    dryRun,
  } = program

  const {
    iGroup, iName, iDescription, iAuthorName, iAuthorEmail,
  } = await inquirer.prompt([
    { name: 'iGroup', message: 'Project group?', when: () => !pGroup },
    { name: 'iName', message: 'Project name?', when: () => !pName },
    { name: 'iDescription', message: 'Project description?', when: () => !pDescription },
    { name: 'iAuthorName', message: 'Author name?', when: () => !pAuthorName && !process.env.AUTHOR_NAME },
    { name: 'iAuthorEmail', message: 'Author email?', when: () => !pAuthorEmail && !process.env.AUTHOR_EMAIL },
  ])

  const group = iGroup || pGroup
  const name = iName || pName
  const description = iDescription || pDescription
  const authorName = iAuthorName || pAuthorName || process.env.AUTHOR_NAME
  const authorEmail = iAuthorEmail || pAuthorEmail || process.env.AUTHOR_EMAIL

  await lib({
    group, name, description, authorName, authorEmail, dryRun,
  })
}

start()
