import React from 'react'
import angular from 'angular'
import { react2angular } from 'react2angular'
import { Provider } from 'mobx-react'

import {{_class}} from './rct-{{name}}.{{type}}'

const stores = {
  // TBD
}

const {{_class}}Wrapper = () => (
  <Provider {...stores} >
    <{{_class}} />
  </Provider>
)

export default angular
  .module('rct-{{name}}-{{type}}', [])
  .component('{{_function}}', react2angular({{_class}}Wrapper))
  .name
