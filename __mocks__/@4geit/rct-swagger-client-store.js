// import debug
import buildDebug from 'debug'
// import hyphenateStyleName from 'hyphenate-style-name'

import userChatboxList from './operations/user-chatbox-list.json'

// prepare debug object
const debug = buildDebug('react-packages:__mocks__:rct-swagger-client-store')
// we generate an manualy mocked object
const rctSwaggerClientStore = jest.genMockFromModule('@4geit/rct-swagger-client-store')

// we mock the buildClient method behavior
// basically doing nothing
rctSwaggerClientStore.buildClient = async () => {
  debug('buildClient mocked!')
}

// we do the same with buildClientWithToken
rctSwaggerClientStore.buildClientWithToken = async () => {
  debug('buildClientWithToken mocked!')
}

// we do the same with buildClientWithAuthorizations
rctSwaggerClientStore.buildClientWithAuthorizations = async () => {
  debug('buildClientWithAuthorizations mocked!')
}

// we mock the object of the `client` member and will mock also
// the underlying functions of each endpoints
rctSwaggerClientStore.client = {
  apis: {
    Account: {
      login: async () => {
        debug('login mocked')
      },
      register: async () => {
        debug('register mocked')
      },
      account: async () => {
        debug('account mocked')
        return {
          body: {
            _id: 0,
            id: 'xxx-yyy-zzz',
            email: 'aaaa@bbbb.xyz',
            token: '123467890',
            firstname: 'aaaa',
            lastname: 'bbbb',
            address: {
              street: 'first street',
              city: 'betterland',
              state: 'poney',
              postcode: '1234',
              country: 'unicorn',
            },
          },
        }
      },
      contactList: async () => {
        debug('contactList mocked')
        return {
          body: [],
        }
      },
      contactAdd: async () => {
        debug('contactAdd mocked')
      },
      contactBulkAdd: async () => {
        debug('contactBulkAdd mocked')
      },
      contactGet: async () => {
        debug('contactGet mocked')
      },
      contactDelete: async () => {
        debug('contactDelete mocked')
      },
      contactUpdate: async () => {
        debug('contactUpdate mocked')
      },
      contactPopulate: async () => {
        debug('contactPopulate mocked')
      },
      productList: async () => {
        debug('productList mocked')
        return {
          body: [],
        }
      },
      productAdd: async () => {
        debug('productAdd mocked')
      },
      productBulkAdd: async () => {
        debug('productBulkAdd mocked')
      },
      productGet: async () => {
        debug('productGet mocked')
      },
      productDelete: async () => {
        debug('productDelete mocked')
      },
      productUpdate: async () => {
        debug('productUpdate mocked')
      },
      productPopulate: async () => {
        debug('productPopulate mocked')
      },
      chatboxList: async () => {
        debug('chatboxList mocked')
        return {
          body: [],
        }
      },
      chatboxAdd: async () => {
        debug('chatboxAdd mocked')
      },
      chatboxBulkAdd: async () => {
        debug('chatboxBulkAdd mocked')
      },
      chatboxGet: async () => {
        debug('chatboxGet mocked')
      },
      chatboxDelete: async () => {
        debug('chatboxDelete mocked')
      },
      chatboxUpdate: async () => {
        debug('chatboxUpdate mocked')
      },
      chatboxPopulate: async () => {
        debug('chatboxPopulate mocked')
      },
      chatboxStatusList: async () => {
        debug('chatboxStatusList mocked')
        return {
          body: [],
        }
      },
      userChatboxList: async ({ maximized }) => {
        debug('userChatboxList mocked')
        debug(`maximized: ${maximized}`)
        if (maximized) {
          return {
            body: [],
          }
        }
        const body = userChatboxList
        debug(body)
        return {
          body,
        }
      },
      userChatboxAdd: async () => {
        debug('userChatboxAdd mocked')
      },
      userChatboxBulkAdd: async () => {
        debug('userChatboxBulkAdd mocked')
      },
      userChatboxGet: async () => {
        debug('userChatboxGet mocked')
      },
      userChatboxDelete: async () => {
        debug('userChatboxDelete mocked')
      },
      userChatboxUpdate: async () => {
        debug('userChatboxUpdate mocked')
      },
      userChatboxBulkUpdate: async () => {
        debug('userChatboxBulkUpdate mocked')
      },
      userChatboxPopulate: async () => {
        debug('userChatboxPopulate mocked')
      },
      eventList: async () => {
        debug('eventList mocked')
        return {
          body: [],
        }
      },
      timeslots: async () => {
        debug('timeslots mocked')
        return {
          body: [],
        }
      },
      messageList: async () => {
        debug('messageList mocked')
        return {
          body: [],
        }
      },
      messageAdd: async () => {
        debug('messageAdd mocked')
      },
      messageBulkAdd: async () => {
        debug('messageBulkAdd mocked')
      },
      messageGet: async () => {
        debug('messageGet mocked')
      },
      messageDelete: async () => {
        debug('messageDelete mocked')
      },
      messageUpdate: async () => {
        debug('messageUpdate mocked')
      },
      messagePopulate: async () => {
        debug('messagePopulate mocked')
      },
      notificationList: async () => {
        debug('notificationList mocked')
        return {
          body: [],
        }
      },
      notificationAdd: async () => {
        debug('notificationAdd mocked')
      },
      notificationBulkAdd: async () => {
        debug('notificationBulkAdd mocked')
      },
      notificationGet: async () => {
        debug('notificationGet mocked')
      },
      notificationDelete: async () => {
        debug('notificationDelete mocked')
      },
      notificationUpdate: async () => {
        debug('notificationUpdate mocked')
      },
      notificationPopulate: async () => {
        debug('notificationPopulate mocked')
      },
      todoList: async () => {
        debug('todoList mocked')
        return {
          body: [],
        }
      },
      todoAdd: async () => {
        debug('todoAdd mocked')
      },
      todoBulkAdd: async () => {
        debug('todoBulkAdd mocked')
      },
      todoGet: async () => {
        debug('todoGet mocked')
      },
      todoDelete: async () => {
        debug('todoDelete mocked')
      },
      todoUpdate: async () => {
        debug('todoUpdate mocked')
      },
      todoPopulate: async () => {
        debug('todoPopulate mocked')
      },
    },
  },
}

export default rctSwaggerClientStore
