import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { storiesOf } from '@storybook/react'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import eventListStore from '@4geit/rct-event-list-store'
import datePickerStore from '@4geit/rct-date-picker-store'

import RctEventListComponent from './rct-event-list.component'

const stores = {
  commonStore,
  swaggerClientStore,
  eventListStore,
  datePickerStore,
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_APP_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_API_APP_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

storiesOf('RctEventListComponent', module)
  .add('simple usage', () => (
    <Provider {...stores} >
      <App>
        <RctEventListComponent
          merchandId={process.env.STORYBOOK_API_APP_TOKEN}
          bookingUrl={process.env.STORYBOOK_BOOKING_URL}
          topTitle="Book your Adventure Today!"
        />
      </App>
    </Provider>
  ))
