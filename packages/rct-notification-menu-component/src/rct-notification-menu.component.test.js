import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationMenuStore from '@4geit/rct-notification-menu-store'

import RctNotificationMenuComponent from './rct-notification-menu.component'

const debug = buildDebug('react-packages:packages:test:rct-notification-menu-component')

const stores = {
  commonStore,
  swaggerClientStore,
  notificationMenuStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctNotificationMenuComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', async () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  await ReactDOM.render(
    <Provider {...stores} >
      <RctNotificationMenuComponent />
    </Provider>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <Provider {...stores} >
//       <RctNotificationMenuComponent />
//     </Provider>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctNotificationMenuComponent {...stores} />)
  expect(wrapper).toMatchSnapshot()
})
