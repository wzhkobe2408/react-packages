import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { inject, observer } from 'mobx-react'

import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import IconButton from 'material-ui/IconButton'
import Icon from 'material-ui/Icon'
import Menu, { MenuItem } from 'material-ui/Menu'
import { ListItemAvatar, ListItemText } from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import Typography from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import Button from 'material-ui/Button'
import Badge from 'material-ui/Badge'
import { CircularProgress } from 'material-ui/Progress'
import moment from 'moment'
import ListSubheader from 'material-ui/List/ListSubheader'
import tinycon from 'tinycon'

import notificationStore from '@4geit/rct-notification-store'

import './rct-notification-menu.component.css'

const debug = buildDebug('react-packages:packages:rct-notification-menu-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  clientName: {
    textDecoration: 'none',
    color: '#2196f3',
  },
  badgeDisabled: {
    display: 'none',
  },
  badge: {
    top: '5px',
    right: '3px',
    height: '20px',
    width: '20px',
  },
  badgeColor: {
    backgroundColor: '#f8981d!important',
  },
  menuItemRoot: {
    height: 110,
    paddingBottom: 0,
    paddingTop: 0,
  },
  listItemTextTypographyRoot: {
    whiteSpace: 'normal',
  },
  menuHeader: {
    padding: 10,
  },
}))
@withWidth()
@inject('notificationMenuStore')
@observer
export default class RctNotificationMenuComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    notificationMenuStore: PropTypes.any.isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    listOperationId: PropTypes.string,
    infoOperationId: PropTypes.string,
    updateOperationId: PropTypes.string,
  }
  static defaultProps = {
    listOperationId: 'notificationList',
    updateOperationId: 'notificationUpdate',
    infoOperationId: 'notificationInfo',
  }

  async componentWillMount() {
    debug('componentWillMount()')
    const { notificationMenuStore, infoOperationId } = this.props
    const { counter } = notificationMenuStore
    try {
      await notificationMenuStore.getHeaders({ infoOperationId })
      tinycon.setBubble(counter)
      this.intervalId = setInterval(async () => {
        await notificationMenuStore.getHeaders({ infoOperationId })
        tinycon.setBubble(counter)
      }, 60000)
    } catch (err) {
      notificationStore.newMessage(err.message)
    }
  }
  componentWillUnmount() {
    debug('componentWillUnmount()')
    clearInterval(this.intervalId)
  }

  intervalId = undefined

  handleLoadMore = async () => {
    debug('handleLoadMore()')
    const { notificationMenuStore, listOperationId } = this.props
    await notificationMenuStore.fetchData({ listOperationId })
  }
  handleClick = async (event) => {
    debug('handleClick()')
    const { notificationMenuStore, listOperationId } = this.props
    notificationMenuStore.setOpen(true)
    notificationMenuStore.setElement(event.currentTarget)
    notificationMenuStore.resetCounter()
    notificationMenuStore.reset()
    tinycon.setBubble(notificationMenuStore.counter)
    // TODO: @geraldinestarke those methods dont exist under the @4geit/rct-notification-menu-store
    await notificationMenuStore.fetchData({ listOperationId })
  }
  // TODO: @geraldinestarke this method is too specific has to move to the wrapper component
  handleNotificationClick = (id, type, link) => async () => {
    debug('handleNotificationClick()')
    const { notificationMenuStore, updateOperationId } = this.props
    await notificationMenuStore.updateClickedData({ updateOperationId, id })
    if (type === 'event') {
      document.location.hash = `#/event-booking/${link}`
      return
    }
    document.location.hash = `#/booking/${link}`
  }
  handleNameClick = (id, type) => () => {
    debug('handleNameClick()')
    if (type === 'event') {
      document.location.hash = `#/guides/${id}`
      return
    }
    document.location.hash = `#/customers/details/${id}`
  }
  handleRequestClose = () => {
    debug('handleRequestClose()')
    this.props.notificationMenuStore.setOpen(false)
  }

  render() {
    debug('render()')
    const { classes } = this.props
    const { notificationMenuStore } = this.props
    const {
      inProgress, typePhrases, typeIcons, totalCount, data, element,
      open, counter, loaded,
    } = notificationMenuStore
    if (!loaded) {
      return (
        <div>
          <IconButton disabled>
            <Icon color="contrast">notifications</Icon>
          </IconButton>
        </div>
      )
    }
    return (
      <div>
        {/* icon button to trigger the menu */}
        {/* with a badge when it contains new items */}
        {/* the badge is hidden when no new items found */}
        <div>
          <Badge
            badgeContent={counter}
            color="accent"
            classes={{
              badge: counter ? classes.badge : classes.badgeDisabled,
              colorAccent: classes.badgeColor,
            }}
          >
            <IconButton onClick={this.handleClick}>
              <Icon color="contrast">notifications</Icon>
            </IconButton>
          </Badge>
        </div>
        {/* we define the menu here */}
        <Menu
          MenuListProps={{
            subheader: (
              <ListSubheader disableSticky>
                <Grid container alignItems="center" justify="space-between">
                  <Grid item>
                    <Typography
                      type="title"
                      classes={{
                        root: classes.menuHeader,
                      }}
                    >
                      Notifications
                    </Typography>
                  </Grid>
                  <Grid item>
                    <IconButton>
                      <Icon onClick={this.handleRequestClose}>close</Icon>
                    </IconButton>
                  </Grid>
                </Grid>
              </ListSubheader>
            ),
          }}
          PaperProps={{
            style: {
              width: 400,
              height: 600,
              overflowX: 'hidden',
            },
          }}
          className="menu"
          anchorEl={element}
          open={open}
          onRequestClose={this.handleRequestClose}
        >
          {/* menu header with a title */}
          {/* we loop over the item available under the data array to display each menu item */}
          { data.map(({
            id, activity, event, payment, refund, isClicked,
            link, timestamp, client, clientId, type, subtype, status,
          }, index) => (
            // we define a menu item
            <MenuItem
              selected={index === 0}
              button
              onClick={this.handleRequestClose}
              divider="true"
              key={id}
              classes={{
                root: classes.menuItemRoot,
              }}
              style={{
                backgroundColor: !isClicked ? '#e6e6e6' : 'none',
              }}
            >
              <ListItemAvatar>
                <Avatar>
                  { type in typeIcons && <Icon>{ typeIcons[type] }</Icon> }
                  { !(type in typeIcons) && <Icon /> }
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                disableTypography
                primary={
                  <Typography
                    classes={{
                      root: classes.listItemTextTypographyRoot,
                    }}
                    type="body1"
                  >
                    { type === 'event' && (
                      <span>Guide, &nbsp;</span>
                    )}
                    <span>
                      <a href="#" onClick={this.handleNameClick(clientId, type)} className={classes.clientName}>{ client }</a> &nbsp;
                    </span>
                    { subtype && type === 'event' && (
                      <span>was { subtype } to &nbsp;</span>
                    )}
                    { type === 'booking' && subtype === 'created' && status === 'paid' && (
                      <span>{ typePhrases.booking } &nbsp;</span>
                    )}
                    { type === 'booking' && subtype === 'created' && status === 'unpaid' && (
                      <span>{ typePhrases.reservation } &nbsp;</span>
                    )}
                    { type === 'booking' && subtype === 'updated' && status !== 'cancelled' && (
                      <span>{ typePhrases.update } &nbsp;</span>
                    )}
                    { type === 'booking' && subtype === 'updated' && status === 'cancelled' && (
                      <span>{ typePhrases.cancellation } &nbsp; </span>
                    )}
                    <span>{ activity } &nbsp;</span>
                    <span>on &nbsp;</span>
                    <span>{ moment(event).format('MMMM Do YYYY, h:mm a') }</span>
                    <span>. &nbsp;</span>
                    { subtype !== 'updated' && payment && payment.length > 0 && type !== 'reservation' && (
                      <span style={{ color: 'green' }}>{ payment }</span>
                    )}
                    { subtype !== 'updated' && payment && payment.length > 0 && type === 'reservation' && (
                      <span style={{ color: 'yellow' }}>{ payment }</span>
                    )}
                    { subtype !== 'updated' && !payment && refund && refund.length > 0 && (
                      <span style={{ color: 'red' }}>{ refund }</span>
                    )}
                  </Typography>
                }
                secondary={
                  <Grid container justify="center" alignItems="center" spacing={40}>
                    <Grid item>
                      <Button
                        dense
                        component="a"
                        color="primary"
                        onClick={this.handleNotificationClick(id, type, link)}
                      >
                        View { type }
                      </Button>
                    </Grid>
                    <Grid item>
                      <Typography type="caption">
                        { moment(timestamp).format('MM/DD/YYYY, h:mm a') }
                      </Typography>
                    </Grid>
                  </Grid>
                }
              />
            </MenuItem>
          )) }
          {/* add a way to load more items */}
          <MenuItem
            style={{ paddingTop: 20 }}
          >
            <Grid container justify="center">
              <Grid item>
                { inProgress && (
                  <CircularProgress />
                ) }
                { !inProgress && data.length === totalCount && (
                  <Typography type="caption">All notifications shown</Typography>
                ) }
                { !inProgress && data.length < totalCount && (
                  <Button color="primary" onClick={this.handleLoadMore} >Load more</Button>
                ) }
              </Grid>
            </Grid>
          </MenuItem>
        </Menu>
      </div>
    )
  }
}
