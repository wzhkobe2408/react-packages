/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line no-unused-vars
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'

import './rct-landing-page-layout.component.css'

const debug = buildDebug('react-packages:packages:rct-landing-page-layout-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  childGrid: {
    maxWidth: 980,
    margin: 'auto',
    paddingLeft: '40px!important',
    paddingRight: '40px!important',
  },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctLandingPageLayoutComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }
  static defaultProps = {
    // TBD
  }

  render() {
    debug('render()')
    const { classes, children } = this.props
    return (
      <Grid container direction="column">
        { children.map((child, index) => (
          <Grid
            item
            key={index}
            style={{
              backgroundImage: `url(${child.props.backgroundImage})`,
              backgroundColor: child.props.backgroundColor,
              paddingTop: 80,
              paddingBottom: 80,
            }}
          >
            <Grid container direction="row" alignItems="center" justify="center">
              <Grid item xs />
              <Grid item classes={{ typeItem: classes.childGrid }}>
                { child }
              </Grid>
              <Grid item xs />
            </Grid>
          </Grid>
        )) }
      </Grid>
    )
  }
}
