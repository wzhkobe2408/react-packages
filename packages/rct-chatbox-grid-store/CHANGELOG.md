# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.89.0"></a>
# [1.89.0](https://gitlab.com/4geit/react-packages/compare/v1.88.0...v1.89.0) (2017-11-29)


### Bug Fixes

* **broadcast-message:** fix issues ([dfbb5c2](https://gitlab.com/4geit/react-packages/commit/dfbb5c2))
* **chatbox-grid:** fix test issue ([c68b3cd](https://gitlab.com/4geit/react-packages/commit/c68b3cd))




<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)


### Bug Fixes

* **chatbox-grid:** fix maximize button issue ([3824b87](https://gitlab.com/4geit/react-packages/commit/3824b87))
* **chatbox-grid:** fix set data ([5b7b7ee](https://gitlab.com/4geit/react-packages/commit/5b7b7ee))




<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.85.1"></a>
## [1.85.1](https://gitlab.com/4geit/react-packages/compare/v1.85.0...v1.85.1) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.84.3"></a>
## [1.84.3](https://gitlab.com/4geit/react-packages/compare/v1.84.2...v1.84.3) (2017-11-16)


### Bug Fixes

* **chatbox-grid:** fix reorderable grid issue ([9b53b12](https://gitlab.com/4geit/react-packages/commit/9b53b12))




<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.83.1"></a>
## [1.83.1](https://gitlab.com/4geit/react-packages/compare/v1.83.0...v1.83.1) (2017-11-03)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.55.0"></a>
# [1.55.0](https://gitlab.com/4geit/react-packages/compare/v1.54.0...v1.55.0) (2017-10-05)


### Features

* **Data table Store:** Add a store method removeItem ([901b541](https://gitlab.com/4geit/react-packages/commit/901b541))




<a name="1.54.0"></a>
# [1.54.0](https://gitlab.com/4geit/react-packages/compare/v1.53.0...v1.54.0) (2017-10-05)


### Features

* **Chatbox Grid Store & Chatbox grid component:** Added an action method in the store and trigger e ([9d15062](https://gitlab.com/4geit/react-packages/commit/9d15062))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.47.0"></a>
# [1.47.0](https://gitlab.com/4geit/react-packages/compare/v1.46.0...v1.47.0) (2017-10-03)


### Bug Fixes

* **ChatboxGrid:** minor changes ([528fcfc](https://gitlab.com/4geit/react-packages/commit/528fcfc))


### Features

* **ChatboxGrid:** Add Onclick directive ([27aa892](https://gitlab.com/4geit/react-packages/commit/27aa892))




<a name="1.44.3"></a>
## [1.44.3](https://gitlab.com/4geit/react-packages/compare/v1.44.2...v1.44.3) (2017-09-27)


### Bug Fixes

* **chatbox-grid:** refactor operation ID prop types ([aa7029d](https://gitlab.com/4geit/react-packages/commit/aa7029d))
* **reorderable-grid-list:** fix setPosition call with destructured param instead of fixed params ([3e64768](https://gitlab.com/4geit/react-packages/commit/3e64768))




<a name="1.42.0"></a>
# [1.42.0](https://gitlab.com/4geit/react-packages/compare/v1.41.0...v1.42.0) (2017-09-22)


### Features

* **rct-chatbox-grid-component:** start integrating reorderable-grid-list-component ([9e4febd](https://gitlab.com/4geit/react-packages/commit/9e4febd))




<a name="1.36.0"></a>
# [1.36.0](https://gitlab.com/4geit/react-packages/compare/v1.35.0...v1.36.0) (2017-09-20)


### Bug Fixes

* minor fix ([c7f4dcd](https://gitlab.com/4geit/react-packages/commit/c7f4dcd))


### Features

* **Chatbox-grid component and store:** Add add a prop to define the API endpoint to call when the c ([f7f997e](https://gitlab.com/4geit/react-packages/commit/f7f997e))




<a name="1.34.4"></a>
## [1.34.4](https://gitlab.com/4geit/react-packages/compare/v1.34.3...v1.34.4) (2017-09-20)


### Bug Fixes

* **chatbox grid:** minor fix ([0d0fcc1](https://gitlab.com/4geit/react-packages/commit/0d0fcc1))
* **chatbox grid store:** minor fix ([6d97706](https://gitlab.com/4geit/react-packages/commit/6d97706))
* **chatbox grid store:** minor fix ([3794e75](https://gitlab.com/4geit/react-packages/commit/3794e75))
* **chatbox grid store:** minor fix ([19a98c9](https://gitlab.com/4geit/react-packages/commit/19a98c9))
* **Chatboxgrid store:** add an activate method ([880ddcb](https://gitlab.com/4geit/react-packages/commit/880ddcb))




<a name="1.33.0"></a>
# [1.33.0](https://gitlab.com/4geit/react-packages/compare/v1.32.2...v1.33.0) (2017-09-19)


### Features

* **Store - Add API endpoint:** Added fetchData, addItem and deleteItem methods ([7e0325b](https://gitlab.com/4geit/react-packages/commit/7e0325b))




<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store

<a name="1.32.1"></a>
## [1.32.1](https://gitlab.com/4geit/react-packages/compare/v1.32.0...v1.32.1) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-store
