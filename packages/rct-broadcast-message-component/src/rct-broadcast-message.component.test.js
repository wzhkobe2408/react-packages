import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import accountStore from '@4geit/rct-account-store'
import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'

import RctBroadcastMessageComponent from './rct-broadcast-message.component'

const debug = buildDebug('react-packages:packages:test:rct-broadcast-message-component')

const stores = {
  accountStore,
  commonStore,
  swaggerClientStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctBroadcastMessageComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores} >
      <RctBroadcastMessageComponent label="test" />
    </Provider>,
    div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount(<RctBroadcastMessageComponent {...stores} label="Send a message" />)
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctBroadcastMessageComponent {...stores} label="Send a message" />)
  expect(wrapper).toMatchSnapshot()
})
