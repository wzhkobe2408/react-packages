import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui-icons/Delete'
import List, { ListItem, ListItemSecondaryAction, ListItemText } from 'material-ui/List'
import Switch from 'material-ui/Switch'
import FullscreenIcon from 'material-ui-icons/Fullscreen'
import FullscreenExitIcon from 'material-ui-icons/FullscreenExit'
import Grid from 'material-ui/Grid'
import Typography from 'material-ui/Typography'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import Avatar from 'material-ui/Avatar'
import Tooltip from 'material-ui/Tooltip'

import { RctChatboxGridStore } from '@4geit/rct-chatbox-grid-store'
import { RctChatboxListStore } from '@4geit/rct-chatbox-list-store'

const debug = buildDebug('react-packages:packages:rct-chatbox-grid-component:item-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // TBD
}))
@withWidth()
@inject('chatboxGridStore', 'chatboxListStore')
@observer
export default class ItemComponent extends Component {
  static propTypes = {
    chatboxGridStore: PropTypes.instanceOf(RctChatboxGridStore).isRequired,
    chatboxListStore: PropTypes.instanceOf(RctChatboxListStore).isRequired,
    updateOperationId: PropTypes.string.isRequired,
    deleteOperationId: PropTypes.string.isRequired,
    listOperationId: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    item: PropTypes.any.isRequired,
    maximized: PropTypes.bool,
  }
  static defaultProps = {
    maximized: false,
  }

  handleMaximize = async () => {
    debug('handleMaximize()')
    const {
      chatboxGridStore, updateOperationId, listOperationId, item,
    } = this.props
    await chatboxGridStore.toggleMaximize({
      updateOperationId,
      itemId: item.id,
      maximized: true,
    })
    await chatboxGridStore.fetchMaximizedItem({ listOperationId })
  }
  handleMinimize = async () => {
    debug('handleMinimize()')
    const {
      chatboxGridStore, updateOperationId, listOperationId, item,
    } = this.props
    await chatboxGridStore.toggleMaximize({
      updateOperationId,
      itemId: item.id,
      maximized: false,
    })
    await chatboxGridStore.fetchMaximizedItem({ listOperationId })
  }
  handleRemoveClick = async () => {
    debug('handleRemoveClick()')
    const {
      chatboxGridStore, chatboxListStore, deleteOperationId,
      listOperationId, item,
    } = this.props
    await chatboxGridStore.deleteItem({
      deleteOperationId,
      itemId: item.id,
    })
    await chatboxGridStore.fetchData({ listOperationId })
    await chatboxListStore.fetchData({ listOperationId: 'chatboxStatusList' })
  }

  render() {
    debug('render()')
    const { item, item: { chatbox: { name }, messages }, maximized } = this.props
    debug(item)
    return (
      <div>
        <Card>
          {/* display messages */}
          <CardContent>
            <List
              style={{
                height: maximized ? '60vh' : 150,
                overflowY: 'scroll',
              }}
            >
              { messages.map(({
                id, message, author, /* isSender, */
              }, index) => {
                const isSender = !!(index % 2)
                return (
                  <ListItem
                    key={id}
                    style={{
                      border: isSender ? '2px solid lightgrey' : '2px solid darkgrey',
                      borderRadius: 10,
                      textAlign: isSender ? 'right' : 'left',
                      margin: 5,
                      padding: 5,
                      paddingRight: isSender ? 30 : 5,
                    }}
                  >
                    { !isSender && (
                      <Avatar
                        style={{
                          backgroundColor: 'darkgrey',
                          width: 30,
                          height: 30,
                        }}
                      >
                        { author.slice(0, 1) }
                      </Avatar>
                    ) }
                    <ListItemText primary={message} />
                    { isSender && (
                      <ListItemSecondaryAction>
                        <Avatar
                          style={{
                            backgroundColor: 'darkgrey',
                            width: 30,
                            height: 30,
                            marginTop: 9,
                            marginRight: 10,
                          }}
                        >
                          { author.slice(0, 1) }
                        </Avatar>
                      </ListItemSecondaryAction>
                    ) }
                  </ListItem>
                )
              }) }
            </List>
          </CardContent>
          {/* chatbox actions */}
          <CardActions>
            <Grid
              container
              alignItems="center"
              style={{
                height: 40,
                paddingLeft: 10,
                backgroundColor: '#83ceec',
              }}
            >
              <Grid item xs>
                <Typography align="left">{ name }</Typography>
              </Grid>
              <Grid item >
                <Tooltip title="Activate" placement="top">
                  <Switch aria-label="checkedA" />
                </Tooltip>
                { !maximized && (
                  <Tooltip title="Maximize" placement="top">
                    <IconButton onClick={this.handleMaximize}>
                      <FullscreenIcon />
                    </IconButton>
                  </Tooltip>
                ) }
                { maximized && (
                  <Tooltip title="Minimize" placement="top">
                    <IconButton onClick={this.handleMinimize}>
                      <FullscreenExitIcon />
                    </IconButton>
                  </Tooltip>
                ) }
                <Tooltip title="Remove" placement="top">
                  <IconButton onClick={this.handleRemoveClick}>
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
      </div>
    )
  }
}
