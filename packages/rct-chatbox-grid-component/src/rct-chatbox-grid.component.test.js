import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'
import FullscreenIcon from 'material-ui-icons/Fullscreen'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import chatboxGridStore from '@4geit/rct-chatbox-grid-store'

import RctReorderableGridListComponent from '@4geit/rct-reorderable-grid-list-component'

import RctChatboxGridComponent from './rct-chatbox-grid.component'
import ItemComponent from './item.component'

const debug = buildDebug('react-packages:packages:test:rct-chatbox-grid-component')

const stores = {
  commonStore,
  swaggerClientStore,
  chatboxGridStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctChatboxGridComponent$/,
  })
}

describe('common', () => {
  debug('common')

  let mount
  let shallow

  beforeEach(() => {
    debug('beforeEach()')
    mount = createMount()
    shallow = createShallow({ untilSelector: 'div' })
  })
  afterEach(() => {
    debug('afterEach()')
    mount.cleanUp()
  })

  it('renders without crashing', () => {
    debug('renders without crashing')
    const div = document.createElement('div')
    ReactDOM.render(
      <Provider {...stores} >
        <RctChatboxGridComponent />
      </Provider>,
      div,
    )
  })
  // it('renders correctly', () => {
  //   debug('renders correctly')
  //   const wrapper = mount((
  //     <Provider {...stores} >
  //       <RctChatboxGridComponent />
  //     </Provider>
  //   ))
  //   expect(wrapper).toMatchSnapshot()
  // })
  it('shallow-renders correctly', () => {
    debug('shallow-renders correctly')
    const wrapper = shallow(<RctChatboxGridComponent {...stores} />)
    expect(wrapper).toMatchSnapshot()
  })
})
describe('maximize a chatbox', () => {
  debug('maximize a chatbox')

  let mount
  let wrapper

  beforeAll(async () => {
    debug('beforeAll()')
    mount = createMount()
    wrapper = await mount((
      <Provider {...stores}>
        <RctChatboxGridComponent />
      </Provider>
    ))
    // wait for the component to be entirely rendered
    await wrapper.update()
    await wrapper.update()
    await wrapper.update()
  })
  afterAll(async () => {
    debug('afterAll()')
    await mount.cleanUp()
  })

  test('given the user is on the dashboard section', () => {
    debug('given the user is on the dashboard section')
    // already on the component
  })
  test('given the user sees at least one item', () => {
    debug('given the user sees at least one item')
    expect((
      wrapper
        .find(RctReorderableGridListComponent)
        .find(ItemComponent)
        .length
    )).toBeGreaterThanOrEqual(4)
  })
  test('when the user clicks on the maximize button of the item', () => {
    debug('when the user clicks on the maximize button of the item')
    // test if FullscreenIcon exists
    expect((
      wrapper
        .find(ItemComponent)
        .first()
        .find(FullscreenIcon)
    )).toHaveLength(1)
    // clicks on the icon
    wrapper
      .find(ItemComponent)
      .first()
      .find(FullscreenIcon)
      .simulate('click')
  })
  test('then the dashboard section should be replaced by maximized chatbox', () => {
    debug('then the dashboard section should be replaced by maximized chatbox')
  })
})
describe('persistent', () => {
  debug('persistent')

  test('given the user maximized a chatbox', () => {
    debug('given the user maximized a chatbox')
  })
  test('when the user reloads the app', () => {
    debug('when the user reloads the app')
  })
  test('then the maximized chatbox should remain on top', () => {
    debug('then the maximized chatbox should remain on top')
  })
})
