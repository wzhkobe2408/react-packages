import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import { RctChatboxGridStore } from '@4geit/rct-chatbox-grid-store'

import RctBroadcastMessageComponent from '@4geit/rct-broadcast-message-component'

import ItemComponent from './item.component'

const debug = buildDebug('react-packages:packages:rct-chatbox-grid-component:maximize-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // TBD
}))
@withWidth()
@inject('chatboxGridStore')
@observer
export default class MaximizeComponent extends Component {
  static propTypes = {
    chatboxGridStore: PropTypes.instanceOf(RctChatboxGridStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    onMessageSubmit: PropTypes.func.isRequired,
  }

  render() {
    debug('render()')
    const { chatboxGridStore } = this.props
    const { maximizedItem } = chatboxGridStore
    // eslint-disable-next-line no-unused-vars
    const { id, chatbox: { name } } = maximizedItem
    return (
      <div>
        <ItemComponent {...this.props} item={maximizedItem} maximized />
        <br />
        <RctBroadcastMessageComponent
          label={`Send a message to ${name}`}
          onSubmit={this.props.onMessageSubmit}
        />
      </div>
    )
  }
}
