# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)


### Bug Fixes

* **side-menu:** remove overflow hidden css property ([96fcf16](https://gitlab.com/4geit/react-packages/commit/96fcf16))




<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.81.2"></a>
## [1.81.2](https://gitlab.com/4geit/react-packages/compare/v1.81.1...v1.81.2) (2017-10-31)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.80.5"></a>
## [1.80.5](https://gitlab.com/4geit/react-packages/compare/v1.80.4...v1.80.5) (2017-10-30)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.80.4"></a>
## [1.80.4](https://gitlab.com/4geit/react-packages/compare/v1.80.3...v1.80.4) (2017-10-30)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.80.1"></a>
## [1.80.1](https://gitlab.com/4geit/react-packages/compare/v1.80.0...v1.80.1) (2017-10-28)


### Bug Fixes

* **notification-menu:** fix ui issues ([35931a7](https://gitlab.com/4geit/react-packages/commit/35931a7))




<a name="1.80.0"></a>
# [1.80.0](https://gitlab.com/4geit/react-packages/compare/v1.79.3...v1.80.0) (2017-10-26)


### Features

* **frontend:** fix side menu ([ea89fc6](https://gitlab.com/4geit/react-packages/commit/ea89fc6))




<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.74.1"></a>
## [1.74.1](https://gitlab.com/4geit/react-packages/compare/v1.74.0...v1.74.1) (2017-10-13)


### Bug Fixes

* **right-side-menu-component:** fix error in sidemenu and content ([579064e](https://gitlab.com/4geit/react-packages/commit/579064e))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.29.0"></a>
# [1.29.0](https://gitlab.com/4geit/react-packages/compare/v1.28.0...v1.29.0) (2017-09-14)


### Features

* **right-side-menu:** implementing right side menu to the main layout component ([afe3f5e](https://gitlab.com/4geit/react-packages/commit/afe3f5e))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/react-packages/compare/v1.26.0...v1.28.0) (2017-09-12)


### Features

* **right-side-menu:** add right-side-menu component with permament type ([004b5ce](https://gitlab.com/4geit/react-packages/commit/004b5ce))




<a name="1.27.0"></a>
# [1.27.0](https://gitlab.com/4geit/react-packages/compare/v1.26.0...v1.27.0) (2017-09-12)


### Features

* **right-side-menu:** add right-side-menu component with permament type ([004b5ce](https://gitlab.com/4geit/react-packages/commit/004b5ce))




<a name="1.23.0"></a>
# [1.23.0](https://gitlab.com/4geit/react-packages/compare/v1.21.0...v1.23.0) (2017-09-08)


### Features

* **sidemenu/footer:** dynamic item icons ([494e731](https://gitlab.com/4geit/react-packages/commit/494e731))




<a name="1.22.0"></a>
# [1.22.0](https://gitlab.com/4geit/react-packages/compare/v1.21.0...v1.22.0) (2017-09-08)


### Features

* **sidemenu/footer:** dynamic item icons ([494e731](https://gitlab.com/4geit/react-packages/commit/494e731))




<a name="1.17.1"></a>
## [1.17.1](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v1.17.1) (2017-09-07)




**Note:** Version bump only for package @4geit/rct-side-menu-component

<a name="1.17.0"></a>
# [1.17.0](https://gitlab.com/4geit/react-packages/compare/v1.16.1...v1.17.0) (2017-09-07)


### Bug Fixes

* **layout:** fix sidemenu display in layout component, remove overflow, fix toolbar elements display ([539f856](https://gitlab.com/4geit/react-packages/commit/539f856))
* **layout:** remove toolbar from header, add header content to sidemenu component and finally use it ([12ca4a6](https://gitlab.com/4geit/react-packages/commit/12ca4a6))
* **layout-component:** minor changes ([99126b7](https://gitlab.com/4geit/react-packages/commit/99126b7))


### Features

* **layout:** add link to side-menu items and login button in header ([60ef6c6](https://gitlab.com/4geit/react-packages/commit/60ef6c6))




<a name="1.16.0"></a>
# [1.16.0](https://gitlab.com/4geit/react-packages/compare/v1.16.1...v1.16.0) (2017-09-07)


### Bug Fixes

* **layout:** fix sidemenu display in layout component, remove overflow, fix toolbar elements display ([539f856](https://gitlab.com/4geit/react-packages/commit/539f856))
* **layout:** remove toolbar from header, add header content to sidemenu component and finally use it ([12ca4a6](https://gitlab.com/4geit/react-packages/commit/12ca4a6))
* **layout-component:** minor changes ([99126b7](https://gitlab.com/4geit/react-packages/commit/99126b7))


### Features

* **layout:** add link to side-menu items and login button in header ([60ef6c6](https://gitlab.com/4geit/react-packages/commit/60ef6c6))




<a name="1.15.4"></a>
## [1.15.4](https://gitlab.com/4geit/react-packages/compare/v1.15.2...v1.15.4) (2017-09-06)


### Bug Fixes

* **side-menu:** fix variable issue ([e67ffb9](https://gitlab.com/4geit/react-packages/commit/e67ffb9))
* **side-menu:** simplified and more elegant code to add props ([1fa47f0](https://gitlab.com/4geit/react-packages/commit/1fa47f0))




<a name="1.15.2"></a>
## [1.15.2](https://gitlab.com/4geit/react-packages/compare/v1.15.0...v1.15.2) (2017-09-05)


### Features

* **side-menu:** add props to side-menu to make it generic ([a665393](https://gitlab.com/4geit/react-packages/commit/a665393))




<a name="1.14.0"></a>
# [1.14.0](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v1.14.0) (2017-09-04)


### Bug Fixes

* **lerna:** fix versioning issue ([eee69c7](https://gitlab.com/4geit/react-packages/commit/eee69c7))


### Features

* **side-menu:** create side-menu component ([d946d44](https://gitlab.com/4geit/react-packages/commit/d946d44))
