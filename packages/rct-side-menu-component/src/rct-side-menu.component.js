import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer } from 'mobx-react'

import classNames from 'classnames'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Drawer from 'material-ui/Drawer'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import Divider from 'material-ui/Divider'
import IconButton from 'material-ui/IconButton'
import MenuIcon from 'material-ui-icons/Menu'
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft'
import Icon from 'material-ui/Icon'

import './rct-side-menu.component.css'

const debug = buildDebug('react-packages:packages:rct-side-menu-component')

const drawerWidth = 240

export const SideMenuItem = ({
  icon, label, divider, route,
}) => (
  <div>
    <ListItem button component={Link} to={route}>
      <ListItemIcon>
        <Icon>{ icon }</Icon>
      </ListItemIcon>
      <ListItemText primary={label} />
    </ListItem>
    { divider && <Divider />}
  </div>
)
SideMenuItem.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  divider: PropTypes.bool,
  route: PropTypes.string.isRequired,
}
SideMenuItem.defaultProps = {
  divider: false,
}

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  root: {
    width: '100%',
    zIndex: 1,
  },
  responsive: {
    [theme.breakpoints.down('lg')]: {
      width: '900px',
    },
    [theme.breakpoints.up('lg')]: {
      width: '1200px',
    },
    margin: 'auto',
    padding: '0 20px',
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100vh',
  },
  appBar: {
    position: 'fixed',
    zIndex: theme.zIndex.navDrawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'fixed',
    height: '100vh',
    overflow: 'hidden',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    width: 60,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  drawerInner: {
    // Make the items inside not wrap when transitioning:
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    height: 56,
    [theme.breakpoints.up('sm')]: {
      height: 64,
    },
  },
  content: {
    width: '100%',
    flexGrow: 1,
    height: 'calc(100vh - 56px)',
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100vh - 64px)',
      marginTop: 64,
    },
  },
  toolbarPaddingLeft: {
    paddingLeft: 20,
  },
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctSideMenuComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    topComponent: PropTypes.any,
    // eslint-disable-next-line react/forbid-prop-types
    contentComponent: PropTypes.any,
    enabled: PropTypes.bool,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }
  static defaultProps = {
    topComponent: <div />,
    contentComponent: <div />,
    enabled: true,
  }

  state = {
    open: false,
  }

  handleDrawerOpen = () => {
    debug('handleDrawerOpen()')
    this.setState({ open: true })
  }
  handleDrawerClose = () => {
    debug('handleDrawerClose()')
    this.setState({ open: false })
  }

  render() {
    debug('render()')
    const {
      classes, children, topComponent, contentComponent, enabled,
    } = this.props
    const {
      root, appFrame, appBar, appBarShift, menuButton, hide, drawerPaper,
      drawerPaperClose, drawerInner, drawerHeader, list, content,
      toolbarPaddingLeft,
    } = classes
    return (
      <div className={root} >
        <div className={appFrame} >
          <AppBar className={classNames(appBar, this.state.open && appBarShift)} >
            <Toolbar
              disableGutters={!this.state.open}
              className={classNames(!enabled && toolbarPaddingLeft)}
            >
              { enabled && (
                <IconButton
                  color="contrast"
                  aria-label="open drawer"
                  onClick={this.handleDrawerOpen}
                  className={classNames(menuButton, this.state.open && hide)}
                >
                  <MenuIcon />
                </IconButton>
              ) }
              { topComponent }
            </Toolbar>
          </AppBar>
          { enabled && (
            <Drawer
              type="permanent"
              classes={{
               paper: classNames(drawerPaper, !this.state.open && drawerPaperClose),
              }}
              open={this.state.open}
            >
              <div className={drawerInner} >
                <div className={drawerHeader}>
                  <IconButton onClick={this.handleDrawerClose}>
                    <ChevronLeftIcon />
                  </IconButton>
                </div>
                <Divider />
                <List className={list}>
                  { children }
                </List>
              </div>
            </Drawer>
          ) }
          <main className={content} >
            { contentComponent }
          </main>
        </div>
      </div>
    )
  }
}
