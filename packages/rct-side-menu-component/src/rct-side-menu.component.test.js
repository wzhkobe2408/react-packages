import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import RctSideMenuComponent, { SideMenuItem } from './rct-side-menu.component'

const debug = buildDebug('react-packages:packages:test:rct-side-menu-component')

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctSideMenuComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <BrowserRouter>
      <RctSideMenuComponent>
        <SideMenuItem icon="inbox" label="Inbox" route="/" />
        <SideMenuItem icon="star" label="Starred" route="/" />
        <SideMenuItem icon="send" label="Send mail" route="/" />
        <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
        <SideMenuItem icon="email" label="All mail" route="/" />
        <SideMenuItem icon="delete" label="Trash" route="/" />
        <SideMenuItem icon="report" label="Spam" route="/" />
      </RctSideMenuComponent>
    </BrowserRouter>,
    div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <BrowserRouter>
//       <RctSideMenuComponent>
//         <SideMenuItem icon="inbox" label="Inbox" route="/" />
//         <SideMenuItem icon="star" label="Starred" route="/" />
//         <SideMenuItem icon="send" label="Send mail" route="/" />
//         <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
//         <SideMenuItem icon="email" label="All mail" route="/" />
//         <SideMenuItem icon="delete" label="Trash" route="/" />
//         <SideMenuItem icon="report" label="Spam" route="/" />
//       </RctSideMenuComponent>
//     </BrowserRouter>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <BrowserRouter>
      <RctSideMenuComponent>
        <SideMenuItem icon="inbox" label="Inbox" route="/" />
        <SideMenuItem icon="star" label="Starred" route="/" />
        <SideMenuItem icon="send" label="Send mail" route="/" />
        <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
        <SideMenuItem icon="email" label="All mail" route="/" />
        <SideMenuItem icon="delete" label="Trash" route="/" />
        <SideMenuItem icon="report" label="Spam" route="/" />
      </RctSideMenuComponent>
    </BrowserRouter>
  ))
  expect(wrapper).toMatchSnapshot()
})
