import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import RctSideMenuComponent, { SideMenuItem } from './rct-side-menu.component'

storiesOf('RctSideMenuComponent', module)
  .add('simple usage', withInfo()(() => (
    <BrowserRouter>
      <RctSideMenuComponent>
        <SideMenuItem icon="inbox" label="Inbox" route="/" />
        <SideMenuItem icon="star" label="Starred" route="/" />
        <SideMenuItem icon="send" label="Send mail" route="/" />
        <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
        <SideMenuItem icon="email" label="All mail" route="/" />
        <SideMenuItem icon="delete" label="Trash" route="/" />
        <SideMenuItem icon="report" label="Spam" route="/" />
      </RctSideMenuComponent>
    </BrowserRouter>
  )))
