# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.44.5"></a>
## [1.44.5](https://gitlab.com/4geit/react-packages/compare/v1.44.4...v1.44.5) (2017-10-02)


### Bug Fixes

* **event-list:** remove debug ([fc13e22](https://gitlab.com/4geit/react-packages/commit/fc13e22))
* **event-list-store:** add missing fields to toAdd() ([748e4d5](https://gitlab.com/4geit/react-packages/commit/748e4d5))
* **event-list-store:** add sorting by hour, and limit data to last event startTime ([f62dd7f](https://gitlab.com/4geit/react-packages/commit/f62dd7f))
* **event-list-store:** add week loop and getNextEvent logic ([d576e0e](https://gitlab.com/4geit/react-packages/commit/d576e0e))
* **event-list-store:** handle case when events list empty ([3c8e5e8](https://gitlab.com/4geit/react-packages/commit/3c8e5e8))
* **event-list-store:** handle case where event doesnt match daysRunning ([0386f89](https://gitlab.com/4geit/react-packages/commit/0386f89))




<a name="1.44.4"></a>
## [1.44.4](https://gitlab.com/4geit/react-packages/compare/v1.44.3...v1.44.4) (2017-09-28)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.40.0"></a>
# [1.40.0](https://gitlab.com/4geit/react-packages/compare/v1.39.4...v1.40.0) (2017-09-22)


### Features

* **event-list-component:** add pagination feature ([dd38138](https://gitlab.com/4geit/react-packages/commit/dd38138))




<a name="1.39.4"></a>
## [1.39.4](https://gitlab.com/4geit/react-packages/compare/v1.39.3...v1.39.4) (2017-09-21)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.39.3"></a>
## [1.39.3](https://gitlab.com/4geit/react-packages/compare/v1.39.2...v1.39.3) (2017-09-21)


### Bug Fixes

* **event-list-store:** use correct parameters of timeslots to filter with date-picker ([a0a8f42](https://gitlab.com/4geit/react-packages/commit/a0a8f42))




<a name="1.39.2"></a>
## [1.39.2](https://gitlab.com/4geit/react-packages/compare/v1.39.1...v1.39.2) (2017-09-21)




**Note:** Version bump only for package @4geit/rct-event-list-store

<a name="1.37.0"></a>
# [1.37.0](https://gitlab.com/4geit/react-packages/compare/v1.36.0...v1.37.0) (2017-09-20)


### Features

* **event-list-component:** improved UI component + integrate new API structure ([e7e0805](https://gitlab.com/4geit/react-packages/commit/e7e0805))




<a name="1.34.3"></a>
## [1.34.3](https://gitlab.com/4geit/react-packages/compare/v1.34.2...v1.34.3) (2017-09-20)


### Bug Fixes

* **event-list-store:** create fechData action method, add componentWillMount method in eventListComp ([970691e](https://gitlab.com/4geit/react-packages/commit/970691e))




<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)


### Bug Fixes

* **eventListComponent:** transform sortedData into a list and iterate over it to display the list of ([ee7f4f9](https://gitlab.com/4geit/react-packages/commit/ee7f4f9))


### Features

* **eventListComponent:** add eventlistComponent and Store as well as function to group events by da ([0e6ec1c](https://gitlab.com/4geit/react-packages/commit/0e6ec1c))
