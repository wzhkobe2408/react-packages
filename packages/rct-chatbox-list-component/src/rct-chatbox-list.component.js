import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import List, { ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction, ListSubheader } from 'material-ui/List'
import ChatIcon from 'material-ui-icons/Chat'
import Switch from 'material-ui/Switch'
import Grid from 'material-ui/Grid'
import { CircularProgress } from 'material-ui/Progress'

import { RctChatboxListStore } from '@4geit/rct-chatbox-list-store'
import { RctChatboxGridStore } from '@4geit/rct-chatbox-grid-store'

import './rct-chatbox-list.component.css'

const debug = buildDebug('react-packages:packages:rct-chatbox-list-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  root: {
    // [theme.breakpoints.down('md')]: {
    //   width: '100%',
    // },
    width: 300,
  },
  // TBD
}))
@withWidth()
@inject('chatboxListStore', 'chatboxGridStore')
@observer
export default class RctChatboxListComponent extends Component {
  static propTypes = {
    chatboxListStore: PropTypes.instanceOf(RctChatboxListStore).isRequired,
    chatboxGridStore: PropTypes.instanceOf(RctChatboxGridStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    listOperationId: PropTypes.string,
    addOperationId: PropTypes.string,
    deleteOperationId: PropTypes.string,
    // TBD
  }
  static defaultProps = {
    listOperationId: 'chatboxStatusList',
    addOperationId: 'userChatboxAdd',
    deleteOperationId: 'userChatboxDelete',
    // TBD
  }

  async componentWillMount() {
    debug('componentWillMount()')
    const { listOperationId } = this.props
    await this.props.chatboxListStore.fetchData({ listOperationId })
  }

  handleSwitchChange = (chatboxId, userChatbox) => async (event, checked) => {
    debug('handleSwitchChange()')
    const {
      chatboxListStore, chatboxGridStore, addOperationId, deleteOperationId,
      listOperationId,
    } = this.props
    if (checked) {
      await chatboxListStore.addUserChatbox({
        addOperationId,
        id: chatboxId,
      })
    } else {
      await chatboxListStore.removeUserChatbox({
        deleteOperationId,
        id: userChatbox.id,
      })
    }
    await chatboxListStore.fetchData({ listOperationId })
    await chatboxGridStore.fetchData({ listOperationId: 'userChatboxList' })
  }

  render() {
    debug('render()')
    const { classes, chatboxListStore } = this.props
    const { chatboxList, inProgress } = chatboxListStore

    if (inProgress) {
      return (
        <Grid container justify="center" alignItems="center" className={classes.root}>
          <Grid item xs style={{ textAlign: 'center' }}>
            <CircularProgress />
          </Grid>
        </Grid>
      )
    }

    return (
      <List
        className={classes.root}
        subheader={<ListSubheader disableSticky>Chatboxes</ListSubheader>}
      >
        { chatboxList.map(({
          chatbox: { id: chatboxId, name, description },
          userChatbox, status,
        }) => (
          <ListItem button key={chatboxId}>
            <ListItemIcon>
              <ChatIcon />
            </ListItemIcon>
            <ListItemText primary={name} secondary={description} />
            <ListItemSecondaryAction>
              <Switch
                checked={status}
                onChange={this.handleSwitchChange(chatboxId, userChatbox)}
              />
            </ListItemSecondaryAction>
          </ListItem>
        )) }
      </List>
    )
  }
}
