import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import Grid from 'material-ui/Grid'
import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'

const debug = buildDebug('react-packages:packages:rct-book-preview-component:check-item')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({

}))
// eslint-disable-next-line react/prefer-stateless-function
export default class SocialButton extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    icon: PropTypes.any.isRequired,
    link: PropTypes.string.isRequired,

  }

  render() {
    debug('render()')
    const { name, icon, link } = this.props
    return (
      <Grid item>
        <Button name={name} href={link}>
          <img src={icon} alt="" />
        </Button>
      </Grid>
    )
  }
}
