import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line no-unused-vars
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'

import './rct-social-buttons.component.css'

const debug = buildDebug('react-packages:packages:rct-social-buttons-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctSocialButtonsComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    children: PropTypes.any.isRequired,
    // TBD
  }
  static defaultProps = {
    // TBD
  }

  render() {
    debug('render()')
    const { children } = this.props
    return (
      <Grid container direction="row">
        { children }
      </Grid>
    )
  }
}
