# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.100.0"></a>
# [1.100.0](https://gitlab.com/4geit/react-packages/compare/v1.99.0...v1.100.0) (2017-12-15)


### Features

* **Social Button component:** Package + Subcomponent ([92ffe4d](https://gitlab.com/4geit/react-packages/commit/92ffe4d))
