# @4geit/rct-social-buttons-component [![npm version](//badge.fury.io/js/@4geit%2Frct-social-buttons-component.svg)](//badge.fury.io/js/@4geit%2Frct-social-buttons-component)

---

As a user I want to get access to the social network feeds

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-social-buttons-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-social-buttons-component) package manager using the following command:

```bash
npm i @4geit/rct-social-buttons-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-social-buttons-component
```

2. Depending on where you want to use the component you will need to import the class `RctSocialButtonsComponent` to your project JS file as follows:

```js
import RctSocialButtonsComponent from '@4geit/rct-social-buttons-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctSocialButtonsComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctSocialButtonsComponent from '@4geit/rct-social-buttons-component'
// ...
const App = () => (
  <div className="App">
    <RctSocialButtonsComponent/>
  </div>
)
```
