import { observable, action } from 'mobx'
import buildDebug from 'debug'
import Swagger from 'swagger-client'

const debug = buildDebug('react-packages:packages:rct-swagger-client-store')

function getUrlTree({ apiUrl }) {
  debug('getUrlTree()')
  const l = document.createElement('a')
  l.href = apiUrl
  return {
    scheme: l.protocol.slice(0, -1),
    host: `${l.hostname}:${l.port}`,
    basePath: l.pathname,
  }
}
async function resolveSwagger({ apiUrl, swaggerUrl }) {
  debug('resolveSwagger()')
  const { spec } = await Swagger.resolve({
    url: `${swaggerUrl}/swagger`,
  })
  // parse api url and override the host and scheme in the swagger client
  const { scheme, host, basePath } = getUrlTree({ apiUrl })
  spec.schemes = [scheme]
  spec.host = host
  spec.basePath = basePath
  return spec
}

export class RctSwaggerClientStore {
  @observable client = undefined

  @action async buildClient({ apiUrl: _apiUrl, swaggerUrl: _swaggerUrl }) {
    debug('buildClient()')
    const apiUrl = _apiUrl || 'http://localhost:10010/v1'
    const swaggerUrl = _swaggerUrl || apiUrl
    const spec = await resolveSwagger({ apiUrl, swaggerUrl })
    this.client = await new Swagger({ spec })
  }
  @action async buildClientWithToken({ token }) {
    debug('buildClientWithToken()')
    this.client = await new Swagger({
      spec: this.client.spec,
      authorizations: {
        AccountSecurity: token,
      },
      // this will force disabling the cache
      requestInterceptor: (requestObj) => {
        const headers = requestObj.headers || {}
        headers['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT'
        headers['Cache-Control'] = 'no-cache'
        headers.Pragma = 'no-cache'
        return requestObj
      },
    })
  }
  @action async buildClientWithAuthorizations({ authorizations }) {
    debug('buildClientWithAuthorizations()')
    this.client = await new Swagger({
      spec: this.client.spec,
      authorizations,
    })
  }
}

export default new RctSwaggerClientStore()
