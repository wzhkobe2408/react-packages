import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import authStore from '@4geit/rct-auth-store'
import notificationStore from '@4geit/rct-notification-store'

import RctLayoutComponent from '@4geit/rct-layout-component'
import RctHeaderComponent from '@4geit/rct-header-component'
import RctSideMenuComponent from '@4geit/rct-side-menu-component'
import RctProjectComponent from './rct-project.component'

const debug = buildDebug('react-packages:packages:test:rct-project-component')

const stores = {
  commonStore,
  swaggerClientStore,
  authStore,
  notificationStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctProjectComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <BrowserRouter>
      <Provider {...stores}>
        <RctProjectComponent
          layoutComponent={
            <RctLayoutComponent
              topComponent={<RctHeaderComponent />}
              sideMenuComponent={
                <RctSideMenuComponent>
                  <div>side menu content</div>
                </RctSideMenuComponent>
              }
            />
          }
        >
          <div>content</div>
        </RctProjectComponent>
      </Provider>
    </BrowserRouter>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <BrowserRouter>
//       <Provider {...stores}>
//         <RctProjectComponent
//           layoutComponent={
//             <RctLayoutComponent
//               topComponent={<RctHeaderComponent />}
//               sideMenuComponent={
//                 <RctSideMenuComponent>
//                   <div>side menu content</div>
//                 </RctSideMenuComponent>
//               }
//             />
//           }
//         >
//           <div>content</div>
//         </RctProjectComponent>
//       </Provider>
//     </BrowserRouter>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <RctProjectComponent
      {...stores}
      layoutComponent={
        <RctLayoutComponent
          topComponent={<RctHeaderComponent />}
          sideMenuComponent={
            <RctSideMenuComponent>
              <div>side menu content</div>
            </RctSideMenuComponent>
          }
        />
      }
    >
      <div>content</div>
    </RctProjectComponent>
  ))
  expect(wrapper).toMatchSnapshot()
})
