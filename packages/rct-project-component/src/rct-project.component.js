import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import './rct-project.component.css'

const debug = buildDebug('react-packages:packages:rct-project-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctProjectComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    layoutComponent: PropTypes.any.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }
  static defaultProps = {
    // TBD
  }

  render() {
    debug('render()')
    const { layoutComponent: _layoutComponent, children } = this.props
    // affect project component children to layout component
    const layoutComponent = _layoutComponent && React.cloneElement(_layoutComponent, {}, children)
    return (
      <div>
        { layoutComponent }
      </div>
    )
  }
}
