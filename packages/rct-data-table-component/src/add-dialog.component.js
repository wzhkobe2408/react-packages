import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer } from 'mobx-react'
import { observable, action } from 'mobx'
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle } from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton'
import Button from 'material-ui/Button'
import AddIcon from 'material-ui-icons/Add'
import TextField from 'material-ui/TextField'
import Tooltip from 'material-ui/Tooltip'

import Column from './column'

const debug = buildDebug('react-packages:packages:rct-data-table-component:add-dialog-component')

@observer
export default class AddDialogComponent extends Component {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
    onSubmit: PropTypes.func.isRequired,
    title: PropTypes.string,
  }
  static defaultProps = {
    title: 'Add a new item',
  }

  componentWillMount() {
    debug('componentWillMount()')
    this.reset()
  }

  @action reset = () => {
    debug('reset()')
    const { columns } = this.props
    columns.forEach(({ name }) => {
      this.fields[name] = ''
    })
    this.open = false
  }
  @action handleOpen = () => {
    debug('handleOpen()')
    this.open = true
  }
  @action handleClose = () => {
    debug('handleClose()')
    this.open = false
  }
  @action handleInputChange = ({ target: { name, value } }) => {
    debug('handleInputChange()')
    this.fields[name] = value
  }
  @action handleOnSubmit = async (event) => {
    debug('handleOnSubmit()')
    event.preventDefault()
    const { onSubmit } = this.props
    await onSubmit(this.fields)
    this.reset()
  }

  @observable fields = {}
  @observable open = false

  render() {
    const { columns, title } = this.props
    return (
      <div>
        {/* add button */}
        <Tooltip title={title}>
          <IconButton onClick={this.handleOpen}>
            <AddIcon />
          </IconButton>
        </Tooltip>
        {/* add form dialog */}
        <Dialog
          open={this.open}
          onRequestClose={this.handleClose}
        >
          <DialogTitle>Add a new item</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Complete the following form and click on the
              Add button in order to add a new item.
            </DialogContentText>
            { columns.map(({ name }) => (
              <TextField
                key={name}
                name={name}
                label={name}
                type="text"
                fullWidth
                onChange={this.handleInputChange}
              />
            ))}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>Cancel</Button>
            <Button onClick={this.handleOnSubmit} raised color="primary">Add</Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
