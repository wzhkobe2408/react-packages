import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import Table, { TableBody, TableCell, TableHead, TableRow, TableFooter, TablePagination } from 'material-ui/Table'
import Checkbox from 'material-ui/Checkbox'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui-icons/Delete'
import Grid from 'material-ui/Grid'
import Tooltip from 'material-ui/Tooltip'

import Column from './column'
import EditDialogComponent from './edit-dialog.component'

const debug = buildDebug('react-packages:packages:rct-data-table-component:table-component')

// eslint-disable-next-line react/prefer-stateless-function
export default class TableComponent extends Component {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
    enabledColumns: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    totalCount: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    perPage: PropTypes.number.isRequired,
    removed: PropTypes.arrayOf(PropTypes.string).isRequired,
    removeId: PropTypes.string.isRequired,
    selected: PropTypes.arrayOf(PropTypes.string).isRequired,
    onRemoveClick: PropTypes.func.isRequired,
    onSelectChange: PropTypes.func.isRequired,
    onSelectAllChange: PropTypes.func.isRequired,
    onEditSubmit: PropTypes.func.isRequired,
    onChangePage: PropTypes.func.isRequired,
    onChangeRowsPerPage: PropTypes.func.isRequired,
  }

  render() {
    debug('render()')
    const {
      columns, enabledColumns, data, totalCount, page, perPage,
      removed, removeId, selected,
      onRemoveClick, onSelectChange, onSelectAllChange, onEditSubmit,
      onChangePage, onChangeRowsPerPage,
    } = this.props
    debug(removed)
    debug(totalCount)
    debug(page)
    debug(perPage)
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell padding="checkbox">
              <Checkbox
                indeterminate={selected.length > 0 && selected.length < data.length}
                checked={selected.length === data.length}
                onChange={onSelectAllChange}
              />
            </TableCell>
            { enabledColumns.map(({ name }) => (
              <TableCell key={name}>{ name }</TableCell>
            )) }
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          { data.map((item) => {
            const { id } = item
            /* eslint-disable no-bitwise */
            const isRemoved = !!~removed.indexOf(id) || removeId === id
            const isSelected = !!~selected.indexOf(id)
            /* eslint-enable no-bitwise */
            return (
              <TableRow hover key={id} style={{ opacity: isRemoved ? 0.5 : 1 }} >
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={isSelected}
                    onChange={onSelectChange({ id })}
                  />
                </TableCell>
                { enabledColumns.map(({ name }) => (
                  <TableCell key={name} >{ item[name] }</TableCell>
                )) }
                <TableCell>
                  <Grid container>
                    <Grid item>
                      {/* edit button */}
                      <EditDialogComponent
                        item={item}
                        columns={columns}
                        onSubmit={onEditSubmit({ id })}
                      />
                    </Grid>
                    <Grid item>
                      {/* delete button */}
                      <Tooltip title="Remove">
                        <IconButton onClick={onRemoveClick({ id })}>
                          <DeleteIcon />
                        </IconButton>
                      </Tooltip>
                    </Grid>
                  </Grid>
                </TableCell>
              </TableRow>
            )
          })}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              count={totalCount}
              page={page}
              rowsPerPage={perPage}
              onChangePage={onChangePage}
              onChangeRowsPerPage={onChangeRowsPerPage}
            />
          </TableRow>
        </TableFooter>
      </Table>
    )
  }
}
