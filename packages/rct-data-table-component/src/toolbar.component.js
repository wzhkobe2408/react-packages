import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import Typography from 'material-ui/Typography'
import Toolbar from 'material-ui/Toolbar'
import DeleteIcon from 'material-ui-icons/Delete'
import IconButton from 'material-ui/IconButton'
import Tooltip from 'material-ui/Tooltip'

import Column from './column'
import ColumnMenuComponent from './column-menu.component'
import AddDialogComponent from './add-dialog.component'
import ImportDialogComponent from './import-dialog.component'

const debug = buildDebug('react-packages:packages:rct-data-table-component:toolbar-component')

const Title = ({ title }) => (
  <Typography
    type="title"
    color="inherit"
    style={{
      flex: 1,
    }}
  >
    { title }
  </Typography>
)
Title.propTypes = {
  title: PropTypes.string.isRequired,
}

// eslint-disable-next-line react/prefer-stateless-function
export default class ToolbarComponent extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    onAddSubmit: PropTypes.func.isRequired,
    onImportSubmit: PropTypes.func.isRequired,
    onRemoveAllClick: PropTypes.func.isRequired,
    columns: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
    removeAllTitle: PropTypes.string,
  }
  static defaultProps = {
    removeAllTitle: 'Remove selected items',
  }

  render() {
    debug('render()')
    const {
      title, columns, removeAllTitle,
      onAddSubmit, onImportSubmit, onRemoveAllClick,
    } = this.props
    return (
      <Toolbar>
        {/* title */}
        <Title title={title} />
        {/* add dialog */}
        <AddDialogComponent onSubmit={onAddSubmit} columns={columns} />
        {/* import dialog */}
        <ImportDialogComponent onSubmit={onImportSubmit} columns={columns} />
        {/* columns button */}
        <ColumnMenuComponent columns={columns} />
        {/* delete button */}
        <Tooltip title={removeAllTitle}>
          <IconButton onClick={onRemoveAllClick}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </Toolbar>
    )
  }
}
