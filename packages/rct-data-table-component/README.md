# @4geit/rct-data-table-component [![npm version](//badge.fury.io/js/@4geit%2Frct-data-table-component.svg)](//badge.fury.io/js/@4geit%2Frct-data-table-component)

---

data table component for react apps

## Demo

A live storybook is available to see how the component looks like @ http://rct-data-table-component.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-data-table-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-data-table-component) package manager using the following command:

```bash
npm i @4geit/rct-data-table-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-data-table-component
```

Make sure you also have those tools installed globally in your system since @4geit/rct-data-table-component requires them to work properly:

* create-react-app: `sudo npm i -g create-react-app`
* storybook: `sudo npm i -g @storybook/cli`

2. Depending on where you want to use the component you will need to import the class `RctDataTableComponent` to your project JS file as follows:

```js
import RctDataTableComponent from '@4geit/rct-data-table-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctDataTableComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctDataTableComponent from '@4geit/rct-data-table-component'
// ...
const App = () => (
  <div className="App">
    <RctDataTableComponent/>
  </div>
)
```

## Usage

1. You can actually use the component `rct-data-table-component` alone by running the command:

```bash
yarn start
```

Make sure the dependencies are all installed by running the command `yarn`.

2. `rct-data-table-component` comes along with `storybook` already setup therefore you can use it by running the command:

```bash
yarn storybook
```

The storybook is also deployed online and available @ http://rct-data-table-component.ws3.4ge.it
