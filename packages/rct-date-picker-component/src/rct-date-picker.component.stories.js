import React from 'react'
import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import datePickerStore from '@4geit/rct-date-picker-store'

import RctDatePickerComponent from './rct-date-picker.component'

const stores = {
  commonStore,
  swaggerClientStore,
  datePickerStore,
}

storiesOf('RctDatePickerComponent', module)
  .add('simple usage', () => (
    <Provider {...stores}>
      <BrowserRouter>
        <RctDatePickerComponent
          months={1}
          placeholder="Choose date"
          calendarIcon
        />
      </BrowserRouter>
    </Provider>
  ))
  .add('two months', () => (
    <Provider {...stores}>
      <BrowserRouter>
        <RctDatePickerComponent
          months={2}
          placeholder="Choose date"
          calendarIcon
        />
      </BrowserRouter>
    </Provider>
  ))
