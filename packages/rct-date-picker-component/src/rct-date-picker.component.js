import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import { SingleDatePicker } from 'react-dates'

import { RctDatePickerStore } from '@4geit/rct-date-picker-store'

import 'react-dates/lib/css/_datepicker.css'

import './rct-date-picker.component.css'

const debug = buildDebug('react-packages:packages:rct-date-picker-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  root: {
    height: 50,
  },
}))
@withWidth()
@inject('datePickerStore')
@observer
export default class RctDatePickerComponent extends Component {
  static propTypes = {
    datePickerStore: PropTypes.instanceOf(RctDatePickerStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    months: PropTypes.number,
    placeholder: PropTypes.string,
    calendarIcon: PropTypes.bool,
  }
  static defaultProps = {
    months: 1,
    placeholder: undefined,
    calendarIcon: true,
  }

  handleDate = (date) => {
    debug('handleDate()')
    this.props.datePickerStore.setDate(date)
  }
  handleFocused = ({ focused }) => {
    debug('handleFocused()')
    this.props.datePickerStore.setFocused(focused)
  }
  render() {
    debug('render()')
    const {
      months, placeholder, calendarIcon, datePickerStore: { date, focused },
    } = this.props
    return (
      <SingleDatePicker
        placeholder={placeholder}
        showDefaultInputIcon={calendarIcon}
        date={date}
        onDateChange={this.handleDate}
        focused={focused}
        onFocusChange={this.handleFocused}
        numberOfMonths={months}
      />
    )
  }
}
