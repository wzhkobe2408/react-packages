import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer } from 'mobx-react'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'

import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import { GridList } from 'material-ui/GridList'

import ReorderableGridTileComponent from './reorderable-grid-tile.component'

import './rct-reorderable-grid-list.component.css'

const debug = buildDebug('react-packages:packages:rct-reorderable-grid-list-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  root: {
    padding: '0 40px',
  },
  // TBD
}))
@withWidth()
@DragDropContext(HTML5Backend)
// @inject('xyzStore')
@observer
export default class RctReorderableGridListComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    handleSetPosition: PropTypes.func.isRequired,
    handleFetchData: PropTypes.func.isRequired,
    updateOperationId: PropTypes.string.isRequired,
    listOperationId: PropTypes.string.isRequired,
    deleteOperationId: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    itemComponent: PropTypes.any.isRequired,
    itemWidth: PropTypes.number,
  }
  static defaultProps = {
    itemWidth: undefined,
    // TBD
  }

  render() {
    debug('render()')
    const {
      handleSetPosition, handleFetchData, updateOperationId,
      listOperationId, deleteOperationId, itemComponent, data,
      itemWidth, ...props
    } = this.props
    return (
      <GridList {...props} >
        { data.map((item, index) => {
          const { id } = item
          return (
            <ReorderableGridTileComponent
              key={id}
              itemId={id}
              item={item}
              position={index}
              handleSetPosition={handleSetPosition}
              handleFetchData={handleFetchData}
              updateOperationId={updateOperationId}
              listOperationId={listOperationId}
              deleteOperationId={deleteOperationId}
              itemComponent={itemComponent}
              style={{ width: itemWidth || 'auto', height: 'auto' }}
            />
          )
        }) }
      </GridList>
    )
  }
}
