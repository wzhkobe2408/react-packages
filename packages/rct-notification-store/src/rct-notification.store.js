import { observable, action } from 'mobx'
import buildDebug from 'debug'

const debug = buildDebug('react-packages:packages:rct-notification-store')

export class RctNotificationStore {
  @observable message = undefined
  @observable duration = 4000
  @observable open = false

  @action setDuration(value) {
    debug('setDuration()')
    this.duration = value
  }
  @action newMessage(value) {
    debug('newMessage()')
    this.message = value
    this.open = true
  }
  @action close() {
    debug('close()')
    this.open = false
  }
}

export default new RctNotificationStore()
