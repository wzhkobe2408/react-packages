import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import Grid from 'material-ui/Grid'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'

const debug = buildDebug('react-packages:packages:rct-book-preview-component:check-item')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({

}))
// eslint-disable-next-line react/prefer-stateless-function
export default class Paragraph extends Component {
  static propTypes = {
    children: PropTypes.string.isRequired,
  }

  render() {
    debug('render()')
    const { children } = this.props
    return (
      <Grid item>
        <Typography type="body2" paragraph>
          { children}
        </Typography>
      </Grid>
    )
  }
}
