import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import centered from '@storybook/addon-centered'
import { withInfo } from '@storybook/addon-info'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'

import RctAuthorComponent from './rct-author.component'
import Paragraph from './paragraph'

import authorPicture from './authorPicture.png'

const stores = {
  commonStore,
  swaggerClientStore,
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

storiesOf('RctAuthorComponent', module)
  .addDecorator(centered)
  .add('simple usage', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores}>
        <App>
          <RctAuthorComponent
            title="A propos de l'auteur"
            authorPicture={authorPicture}
            author="Wolf Starke"
          >
            <Paragraph>
              Né dans les tourbillons de l après-guerre 1945, dans un pavillon
              de chasse en pleine forêt du nord de l Allemagne, j ai pris mes
              racines avec mère nature y passant mon enfance loin de toute
              civilisation.
            </Paragraph>
            <Paragraph>
              Ensuite, j ai pris des chemins plus conventionnels et suis devenu
              avocat avec mon propre cabinet.
            </Paragraph>
            <Paragraph>
              Au fil des ans, évoluant de par mes déplacements sur divers
              continents, je me suis spécialisé dans le droit des affaires
              international. Mon intérêt pour l histoire, la littérature et la
              culture, se sont intensifiés et précisés surtout lors de mes
              séjours au Maroc et en Égypte.
            </Paragraph>
            <Paragraph>
              L Occidental tombant sous le charme de l Orient ! Installé depuis
              peu à Bruxelles, je peux mettre à profit mes expériences dans
              l écriture, la photographie et la peinture.
            </Paragraph>
          </RctAuthorComponent>
        </App>
      </Provider>
    </BrowserRouter>
  )))
