# @4geit/rct-author-component [![npm version](//badge.fury.io/js/@4geit%2Frct-author-component.svg)](//badge.fury.io/js/@4geit%2Frct-author-component)

---

component to show author details, biographie

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-author-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-author-component) package manager using the following command:

```bash
npm i @4geit/rct-author-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-author-component
```

2. Depending on where you want to use the component you will need to import the class `RctAuthorComponent` to your project JS file as follows:

```js
import RctAuthorComponent from '@4geit/rct-author-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctAuthorComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctAuthorComponent from '@4geit/rct-author-component'
// ...
const App = () => (
  <div className="App">
    <RctAuthorComponent/>
  </div>
)
```
