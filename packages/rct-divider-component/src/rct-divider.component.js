import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import './rct-divider.component.css'

const debug = buildDebug('react-packages:packages:rct-divider-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctDividerComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    lineColor: PropTypes.string,
  }
  static defaultProps = {
    lineColor: '#d8d9d9',
  }

  render() {
    debug('render()')
    const { lineColor } = this.props
    return (
      <hr style={{
        margin: '0 10px',
        border: 'none',
        borderBottomColor: lineColor,
        borderBottomStyle: 'dashed',
        borderBottomWidth: '2px',
      }}
      />
    )
  }
}
