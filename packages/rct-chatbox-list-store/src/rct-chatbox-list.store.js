import { observable, action, runInAction } from 'mobx'
import buildDebug from 'debug'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

const debug = buildDebug('react-packages:packages:rct-chatbox-list-store')

export class RctChatboxListStore {
  @observable inProgress = false
  @observable chatboxList = []

  @action setChatboxList(value) {
    debug('setChatboxList()')
    this.chatboxList = value
  }
  @action async fetchData({ listOperationId }) {
    debug('fetchData()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[listOperationId || 'chatboxStatusList']()
      debug(body)
      runInAction(() => {
        if (body.length) {
          this.setChatboxList(body)
        }
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async addUserChatbox({ addOperationId, id }) {
    debug('addUserChatbox()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[addOperationId]({
        body: {
          chatboxId: id,
        },
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async removeUserChatbox({ deleteOperationId, id }) {
    debug('removeUserChatbox()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[deleteOperationId]({ id })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctChatboxListStore()
