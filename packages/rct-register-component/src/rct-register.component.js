import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { inject, observer } from 'mobx-react'
import { withRouter } from 'react-router-dom'

import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import VpnKeyIcon from 'material-ui-icons/VpnKey'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import Typography from 'material-ui/Typography'
import { FormControlLabel } from 'material-ui/Form'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import { RctAuthStore } from '@4geit/rct-auth-store'

import './rct-register.component.css'

const debug = buildDebug('react-packages:packages:rct-register-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // TBD
}))
@withWidth()
@inject('authStore')
@withRouter
@observer
export default class RctRegisterComponent extends Component {
  static propTypes = {
    authStore: PropTypes.instanceOf(RctAuthStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    history: PropTypes.object.isRequired,
    cardWidth: PropTypes.string,
    redirectTo: PropTypes.string,
  }
  static defaultProps = {
    cardWidth: '100%',
    redirectTo: '/',
  }

  componentWillUnmount() {
    debug('componentWillUnmount()')
    this.props.authStore.reset()
  }
  handleEmailChange = (event) => {
    debug('handleEmailChange()')
    this.props.authStore.setEmail(event.target.value)
  }
  handlePasswordChange = (event) => {
    debug('handlePasswordChange()')
    this.props.authStore.setPassword(event.target.value)
  }
  handleFirstnameChange = (event) => {
    debug('handleFirstnameChange()')
    this.props.authStore.setFirstname(event.target.value)
  }
  handleLastnameChange = (event) => {
    debug('handleLastnameChange()')
    this.props.authStore.setLastname(event.target.value)
  }
  handleAcknowledgeChange = (event, isInputChecked) => {
    debug('handleAcknowledgeChange()')
    this.props.authStore.setAcknowledge(isInputChecked)
  }
  handleSubmitForm = async (event) => {
    debug('handleSubmitForm()')
    event.preventDefault()
    await this.props.authStore.register()
    this.props.history.replace(this.props.redirectTo)
  }
  render() {
    debug('render()')
    const { cardWidth } = this.props
    const { values, inProgress } = this.props.authStore
    return (
      <div style={{
        width: cardWidth || '100%',
      }}
      >
        <form onSubmit={this.handleSubmitForm}>
          <Card>
            <CardHeader
              title="Registration"
              subheader="Create your account"
              avatar={<VpnKeyIcon />}
            />
            <CardContent>
              <Typography type="body1" gutterBottom>
                Fill out the fields above to create a new account.
              </Typography>
              <div>
                <TextField
                  label="Email"
                  type="email"
                  id="email"
                  name="email"
                  value={values.email}
                  onChange={this.handleEmailChange}
                  fullWidth
                  required
                  autoFocus
                />
              </div>
              <div>
                <TextField
                  label="Password"
                  type="password"
                  id="password"
                  name="password"
                  value={values.password}
                  onChange={this.handlePasswordChange}
                  fullWidth
                  required
                />
              </div>
              <div>
                <TextField
                  label="Firstname"
                  type="text"
                  id="firstname"
                  name="firstname"
                  value={values.firstname}
                  onChange={this.handleFirstnameChange}
                  fullWidth
                />
              </div>
              <div>
                <TextField
                  label="Lastname"
                  type="text"
                  id="lastname"
                  name="lastname"
                  value={values.lastname}
                  onChange={this.handleLastnameChange}
                  fullWidth
                />
              </div>
              <div>
                <FormControlLabel
                  control={
                    <Checkbox
                      id="acknowledge"
                      name="acknowledge"
                      checked={values.acknowledge}
                      onChange={this.handleAcknowledgeChange}
                    />
                  }
                  label="I agree with the registration terms"
                />
              </div>
            </CardContent>
            <CardActions>
              <Grid container justify="center">
                <Grid item>
                  <Button
                    raised
                    color="accent"
                    type="submit"
                    disabled={inProgress}
                  >
                    Sign up
                  </Button>
                </Grid>
              </Grid>
            </CardActions>
          </Card>
        </form>
      </div>
    )
  }
}
