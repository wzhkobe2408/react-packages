import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import authStore from '@4geit/rct-auth-store'
import notificationStore from '@4geit/rct-notification-store'

import RctRegisterComponent from './rct-register.component'

const debug = buildDebug('react-packages:packages:test:rct-register-component')

const stores = {
  commonStore,
  authStore,
  notificationStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctRegisterComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <BrowserRouter>
      <Provider {...stores} >
        <RctRegisterComponent />
      </Provider>
    </BrowserRouter>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <BrowserRouter>
//       <RctRegisterComponent {...stores} />
//     </BrowserRouter>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <BrowserRouter>
      <RctRegisterComponent {...stores} />
    </BrowserRouter>
  ))
  expect(wrapper).toMatchSnapshot()
})
