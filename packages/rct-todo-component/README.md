# @4geit/rct-todo-component [![npm version](//badge.fury.io/js/@4geit%2Frct-todo-component.svg)](//badge.fury.io/js/@4geit%2Frct-todo-component)

---

todo

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-todo-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-todo-component) package manager using the following command:

```bash
npm i @4geit/rct-todo-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-todo-component
```

2. Depending on where you want to use the component you will need to import the class `RctTodoComponent` to your project JS file as follows:

```js
import RctTodoComponent from '@4geit/rct-todo-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctTodoComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctTodoComponent from '@4geit/rct-todo-component'
// ...
const App = () => (
  <div className="App">
    <RctTodoComponent/>
  </div>
)
```
