# @4geit/rct-matrix-component [![npm version](//badge.fury.io/js/@4geit%2Frct-matrix-component.svg)](//badge.fury.io/js/@4geit%2Frct-matrix-component)

---

matrix to render the convay's game board

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-matrix-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-matrix-component) package manager using the following command:

```bash
npm i @4geit/rct-matrix-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-matrix-component
```

2. Depending on where you want to use the component you will need to import the class `RctMatrixComponent` to your project JS file as follows:

```js
import RctMatrixComponent from '@4geit/rct-matrix-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctMatrixComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctMatrixComponent from '@4geit/rct-matrix-component'
// ...
const App = () => (
  <div className="App">
    <RctMatrixComponent/>
  </div>
)
```
