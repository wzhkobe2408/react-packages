import { RctAccountStore } from './rct-account.store'

describe('RctAccountStore', () => {
  it('set body', () => {
    const store = new RctAccountStore()
    store.setBody({ testField: 'testValue' })
    expect(store.body).toHaveProperty('testField', 'testValue')
  })
})
