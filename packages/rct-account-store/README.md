# @4geit/rct-account-store [![npm version](//badge.fury.io/js/@4geit%2Frct-account-store.svg)](//badge.fury.io/js/@4geit%2Frct-account-store)

---

reusable account store package

## Demo

A live storybook is available to see how the store looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-account-store*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-account-store) package manager using the following command:

```bash
npm i @4geit/rct-account-store --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-account-store
```

2. Depending on where you want to use the store you will need to import the class instance `accountStore` or inject it to your project JS file.

If you are willing to use it within a component, then you must use the `inject` decorator provided by `mobx-react` library.

For instance if you want to use this store in your `App.js` component, you can use the RctAccountStore store in the JSX code as follows:

```js
import React, { Component } from 'react'
import { inject } from 'mobx-react'
// ...
@inject('accountStore')
class App extends Component {
  handleClick() {
    this.props.accountStore.setVar1('dummy value')
  }

  render() {
    return (
      <div className="App">
        <button onClick={ this.handleClick.bind(this) } >Click here</button>
      </div>
    )
  }
}
```

If you are willing to use the class instance inside another store class, then you can just import the instance as follows:

```js
import accountStore from '@4geit/rct-account-store'

class DummyStore {
  @action doSomething() {
    accountStore.setVar1('dummy value')
  }
}
```

3. If you want to use the store class in the storybook, add accountStore within `stories/index.js` by first importing it:

```js
import accountStore from '@4geit/rct-account-store'
```

and then within the `stores` array variable add `accountStore` at the end of the list.

4. If you want to use the store class in your project, add accountStore within `src/index.js` by first importing it:

```js
import accountStore from '@4geit/rct-account-store'
```

and then within the `stores` array variable add `accountStore` at the end of the list.
