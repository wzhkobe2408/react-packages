import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { inject, observer } from 'mobx-react'

import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import TextField from 'material-ui/TextField'
import Grid from 'material-ui/Grid'
import AccountBoxIcon from 'material-ui-icons/AccountBox'

import { RctAccountStore } from '@4geit/rct-account-store'

import './rct-account.component.css'

const debug = buildDebug('react-packages:packages:rct-account-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
@inject('accountStore', 'commonStore')
@observer
export default class RctAccountComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    commonStore: PropTypes.any.isRequired,
    accountStore: PropTypes.instanceOf(RctAccountStore).isRequired,
    updateOperationId: PropTypes.string,
    cardWidth: PropTypes.string,
  }
  static defaultProps = {
    updateOperationId: 'userUpdate',
    cardWidth: '400px',
  }

  handleAccountChange = (event) => {
    debug('handleAccountChange()')
    this.props.accountStore.setBodyField(event.target.name, event.target.value)
  }
  handleUpdate = () => {
    debug('handleUpdate()')
    const { updateOperationId, accountStore } = this.props
    accountStore.userUpdate({ updateOperationId })
  }
  render() {
    debug('render()')
    const { cardWidth } = this.props
    const { user } = this.props.commonStore

    debug('user: %O', user)

    if (!user) {
      return (
        <Typography>Loading...</Typography>
      )
    }

    const {
      firstname, lastname, email, password, company, phone, address: {
        street, city, state, postcode, country,
      },
    } = user

    debug(firstname)

    return (
      <div style={{
        width: cardWidth || '100%',
      }}
      >
        <Card>
          <CardHeader
            style={{
              backgroundColor: '#2196f3',
              color: 'white',
            }}
            title="My Account"
            avatar={<AccountBoxIcon />}
          />
          <CardContent>
            <Typography type="body1" gutterBottom>
              Fill out the fields above with your information.
            </Typography>
            <div>
              <TextField
                label="Email"
                type="email"
                id="email"
                name="email"
                value={email}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
            </div>
            <div>
              <TextField
                label="Password"
                type="password"
                id="password"
                name="password"
                value={password}
                onChange={this.handleAccountChange}
                fullWidth
                required
              />
            </div>
            <div>
              <TextField
                label="Firstname"
                type="firstname"
                id="firstname"
                name="firstname"
                value={firstname}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
            </div>
            <div>
              <TextField
                label="Lastname"
                type="lastname"
                id="lastname"
                name="lastname"
                value={lastname}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
            </div>
            <div>
              <TextField
                label="Company"
                type="company"
                id="company"
                name="company"
                value={company}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
            </div>
            <div>
              <TextField
                label="Street"
                type="street"
                id="street"
                name="street"
                value={street}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
              <TextField
                label="City"
                type="city"
                id="city"
                name="city"
                value={city}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
              <TextField
                label="State"
                type="state"
                id="state"
                name="state"
                value={state}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
              <TextField
                label="Postcode"
                type="postcode"
                id="postcode"
                name="postcode"
                value={postcode}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
              <TextField
                label="Country"
                type="country"
                id="country"
                name="country"
                value={country}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
            </div>
            <div>
              <TextField
                label="Phone"
                type="phone"
                id="phone"
                name="phone"
                value={phone}
                onChange={this.handleAccountChange}
                fullWidth
                required
                autoFocus
              />
            </div>
          </CardContent>
          <CardActions>
            <Grid container justify="center">
              <Grid item>
                <Button
                  raised
                  color="primary"
                  type="submit"
                >
                  Update
                </Button>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
      </div>
    )
  }
}
