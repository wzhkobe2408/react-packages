# @4geit/rct-right-side-menu-component [![npm version](//badge.fury.io/js/@4geit%2Frct-right-side-menu-component.svg)](//badge.fury.io/js/@4geit%2Frct-right-side-menu-component)

---

Right side-menu component to use along with the layout component

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-right-side-menu-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-right-side-menu-component) package manager using the following command:

```bash
npm i @4geit/rct-right-side-menu-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-right-side-menu-component
```

2. Depending on where you want to use the component you will need to import the class `RctRightSideMenuComponent` to your project JS file as follows:

```js
import RctRightSideMenuComponent from '@4geit/rct-right-side-menu-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctRightSideMenuComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctRightSideMenuComponent from '@4geit/rct-right-side-menu-component'
// ...
const App = () => (
  <div className="App">
    <RctRightSideMenuComponent/>
  </div>
)
```
