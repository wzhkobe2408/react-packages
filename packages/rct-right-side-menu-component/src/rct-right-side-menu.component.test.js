import React from 'react'
import ReactDOM from 'react-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import RctRightSideMenuComponent from './rct-right-side-menu.component'

const debug = buildDebug('react-packages:packages:test:rct-right-side-menu-component')

process.env.STORYBOOK_TOKEN = '1234'
process.env.STORYBOOK_BOOKING_URL = 'https://'

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctRightSideMenuComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <RctRightSideMenuComponent>
      <div />
    </RctRightSideMenuComponent>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <RctRightSideMenuComponent>
//       <div />
//     </RctRightSideMenuComponent>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <RctRightSideMenuComponent>
      <div />
    </RctRightSideMenuComponent>
  ))
  expect(wrapper).toMatchSnapshot()
})
