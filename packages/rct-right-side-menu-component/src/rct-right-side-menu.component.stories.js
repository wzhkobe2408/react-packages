import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import Typography from 'material-ui/Typography'
import List, { ListItem, ListItemText } from 'material-ui/List'

import commonStore from '@4geit/rct-common-store'
import notificationStore from '@4geit/rct-notification-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import chatboxListStore from '@4geit/rct-chatbox-list-store'
import eventListStore from '@4geit/rct-event-list-store'
import datePickerStore from '@4geit/rct-date-picker-store'

import RctChatboxListComponent from '@4geit/rct-chatbox-list-component'
import RctEventListComponent from '@4geit/rct-event-list-component'

import RctRightSideMenuComponent from './rct-right-side-menu.component'

const stores = {
  commonStore,
  notificationStore,
  swaggerClientStore,
  chatboxListStore,
  eventListStore,
  datePickerStore,
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

/* eslint-disable react/no-multi-comp */
@inject('swaggerClientStore', 'commonStore')
@observer
class APIApp extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_APP_URL || 'http://localhost:10010/v1',
        swaggerUrl: process.env.STORYBOOK_SWAGGER_URL,
      })
      await swaggerClientStore.buildClientWithAuthorizations({
        authorizations: {
          TimeslotSecurity: process.env.STORYBOOK_API_APP_TOKEN,
        },
      })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}
/* eslint-enable react/no-multi-comp */

storiesOf('RctRightSideMenuComponent', module)
  .add('simple usage', withInfo()(() => (
    <BrowserRouter>
      <RctRightSideMenuComponent
        contentComponent={
          <Typography type="body1" noWrap>
            You think water moves fast? You should see ice.
          </Typography>
        }
      >
        <List>
          <ListItem button>
            <ListItemText primary="chatbox1" />
          </ListItem>
          <ListItem button>
            <ListItemText primary="chatbox2" />
          </ListItem>
          <ListItem button>
            <ListItemText primary="chatbox3" />
          </ListItem>
          <ListItem button>
            <ListItemText primary="chatbox4" />
          </ListItem>
        </List>
      </RctRightSideMenuComponent>
    </BrowserRouter>
  )))
  .add('with chatboxList', withInfo()(() => (
    <Provider {...stores}>
      <App>
        <RctRightSideMenuComponent
          contentComponent={
            <Typography type="body1" noWrap>
              You think water moves fast? You should see ice.
            </Typography>
          }
        >
          <RctChatboxListComponent />
        </RctRightSideMenuComponent>
      </App>
    </Provider>
  )))
  .add('with eventList', withInfo()(() => (
    <Provider {...stores} >
      <APIApp>
        <RctRightSideMenuComponent
          contentComponent={
            <Typography type="body1" noWrap>
              You think water moves fast? You should see ice.
            </Typography>
          }
        >
          <RctEventListComponent
            merchandId={process.env.STORYBOOK_TOKEN}
            bookingUrl={process.env.STORYBOOK_BOOKING_URL}
            topTitle="Book your Adventure Today!"
          />
        </RctRightSideMenuComponent>
      </APIApp>
    </Provider>
  )))
