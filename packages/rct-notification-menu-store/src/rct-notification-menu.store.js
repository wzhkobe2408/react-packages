import { observable, action, runInAction } from 'mobx'
import buildDebug from 'debug'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

const debug = buildDebug('react-packages:packages:rct-notification-menu-store')

export class RctNotificationMenuStore {
  @observable inProgress = false
  @observable open = false
  @observable element = undefined
  @observable page = 1
  @observable counter = 5
  @observable data = []
  @observable totalCount = 0
  @observable typePhrases = {
    booking: 'booked',
    adjustment: 'booking adjusted for',
    refund: 'refunded for',
    'partially refunded': 'partially refunded for',
    reservation: 'reserved',
    cancellation: 'cancelled',
    update: 'updated',
  }
  @observable typeIcons = {
    booking: 'date_range',
    adjustement: 'assignment',
    refund: 'account_balance',
    'partially refunded': 'payment',
    reservation: 'card_travel',
    guide: 'person_pin',
    cancellation: 'remove_cirle_outline',
  }
  @observable loaded = true

  @action resetCounter() {
    debug('resetCounter()')
    this.counter = 0
  }
  @action reset() {
    debug('reset()')
    this.data = []
    this.page = 1
  }
  @action setOpen(value) {
    debug('setOpen()')
    this.open = value
  }
  @action setElement(value) {
    debug('setElement()')
    this.element = value
  }
  @action appendData(value) {
    debug('appendData()')
    this.data = this.data || []
    this.data = this.data.concat(value)
    this.page += 1
    debug(this.data)
  }
  @action async fetchData({ listOperationId }) {
    debug('fetchData()')
    this.inProgress = true
    try {
      const { body, headers: { 'x-totalcount': _totalCount } } = await swaggerClientStore.client.apis.Account[listOperationId]({
        pageSize: 10,
        page: this.page,
      })
      if (body && body.length) {
        runInAction(() => {
          this.totalCount = parseInt(_totalCount, 10)
          this.appendData(body)
        })
      }
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  /* eslint-disable class-methods-use-this */
  @action updateReadData() {
    debug('updateReadData()')
    // do nothing
  }
  @action async getHeaders() {
    debug('getHeaders()')
    // do something
  }
  /* eslint-enable class-methods-use-this */
}

export default new RctNotificationMenuStore()
