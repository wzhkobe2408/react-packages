import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

// import xyzStore from '@4geit/rct-xyz-store'

import RctBookPreviewComponent, { CheckItem } from './rct-book-preview.component'

import fbCover from './fbCover.png'

const debug = buildDebug('react-packages:packages:test:rct-book-preview-component')

const stores = {
  // xyzStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctBookPreviewComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores} >
      <RctBookPreviewComponent
        button="Commander"
        display="Le Flash Bruxellois"
        title="Pré-commande de Noël maintenant disponible! Réservez vite le vôtre !"
        fbCover={fbCover}
      >
        <CheckItem label="40 Flashs bruxellois en un livre unique" />
        <CheckItem label="illustrations en couleurs" />
        <CheckItem label="Préface par Dr. Ghada el Wakil" />
        <CheckItem label="Livre dédicacé par l'auteur" />
        <CheckItem label="Contenu inédit et petites surprises" />
      </RctBookPreviewComponent>
    </Provider>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <RctBookPreviewComponent
//       button="Commander"
//       display="Le Flash Bruxellois"
//       title="Pré-commande de Noël maintenant disponible! Réservez vite le vôtre !"
//       fbCover={fbCover}
//     >
//       <CheckItem label="40 Flashs bruxellois en un livre unique" />
//       <CheckItem label="illustrations en couleurs" />
//       <CheckItem label="Préface par Dr. Ghada el Wakil" />
//       <CheckItem label="Livre dédicacé par l'auteur" />
//       <CheckItem label="Contenu inédit et petites surprises" />
//     </RctBookPreviewComponent>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <RctBookPreviewComponent
      button="Commander"
      display="Le Flash Bruxellois"
      title="Pré-commande de Noël maintenant disponible! Réservez vite le vôtre !"
      fbCover={fbCover}
    >
      <CheckItem label="40 Flashs bruxellois en un livre unique" />
      <CheckItem label="illustrations en couleurs" />
      <CheckItem label="Préface par Dr. Ghada el Wakil" />
      <CheckItem label="Livre dédicacé par l'auteur" />
      <CheckItem label="Contenu inédit et petites surprises" />
    </RctBookPreviewComponent>
  ))
  expect(wrapper).toMatchSnapshot()
})
