# @4geit/rct-book-preview-component [![npm version](//badge.fury.io/js/@4geit%2Frct-book-preview-component.svg)](//badge.fury.io/js/@4geit%2Frct-book-preview-component)

---

As a user I want to have a preview of the book right when I arrive on the website at a glance

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-book-preview-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-book-preview-component) package manager using the following command:

```bash
npm i @4geit/rct-book-preview-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-book-preview-component
```

2. Depending on where you want to use the component you will need to import the class `RctBookPreviewComponent` to your project JS file as follows:

```js
import RctBookPreviewComponent from '@4geit/rct-book-preview-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctBookPreviewComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctBookPreviewComponent from '@4geit/rct-book-preview-component'
// ...
const App = () => (
  <div className="App">
    <RctBookPreviewComponent/>
  </div>
)
```
