import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import notificationStore from '@4geit/rct-notification-store'

import RctNotificationComponent from './rct-notification.component'

const debug = buildDebug('react-packages:packages:test:rct-notification-component')

const stores = {
  notificationStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctNotificationComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores} >
      <RctNotificationComponent />
    </Provider>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <Provider {...stores} >
//       <RctNotificationComponent />
//     </Provider>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctNotificationComponent {...stores} />)
  expect(wrapper).toMatchSnapshot()
})
