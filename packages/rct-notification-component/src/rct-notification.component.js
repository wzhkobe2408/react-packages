import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Snackbar from 'material-ui/Snackbar'

import { RctNotificationStore } from '@4geit/rct-notification-store'

import './rct-notification.component.css'

const debug = buildDebug('react-packages:packages:rct-notification-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
@inject('notificationStore')
@observer
export default class RctNotificationComponent extends Component {
  static propTypes = {
    notificationStore: PropTypes.instanceOf(RctNotificationStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
  }
  static defaultProps = {
    // TBD
  }

  handleSnackbar = () => {
    debug('handleSnackbar()')
    this.props.notificationStore.close()
  }
  render() {
    debug('render()')
    const { open, message, duration } = this.props.notificationStore
    return (
      <Snackbar
        open={open}
        message={message || 'no message'}
        autoHideDuration={duration}
        onRequestClose={this.handleSnackbar}
      />
    )
  }
}
