import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'
import IconButton from 'material-ui/IconButton'
import Tooltip from 'material-ui/Tooltip'
import Typography from 'material-ui/Typography'
import { FormGroup, FormControlLabel } from 'material-ui/Form'
import Checkbox from 'material-ui/Checkbox'
import PlayCircleFilledIcon from 'material-ui-icons/PlayCircleFilled'
import PauseCircleFilledIcon from 'material-ui-icons/PauseCircleFilled'
import SettingsIcon from 'material-ui-icons/Settings'
import NavigateBeforeIcon from 'material-ui-icons/NavigateBefore'
import Slider from 'rc-slider'
import 'rc-slider/assets/index.css'

import RctMatrixComponent from '@4geit/rct-matrix-component'

import './rct-conway.component.css'

const debug = buildDebug('react-packages:packages:rct-conway-component')

const Board = ({ nrows, ncols, board }) => (
  <Grid
    item
    style={{
      width: ncols * 10,
      height: nrows * 10,
      padding: 0,
    }}
  >
    <RctMatrixComponent
      nrows={nrows}
      ncols={ncols}
      board={board}
    />
  </Grid>
)
Board.propTypes = {
  nrows: PropTypes.number.isRequired,
  ncols: PropTypes.number.isRequired,
  board: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.bool)).isRequired,
}

const Play = ({
  backgroundImage, nrows, ncols, onPlayClick, onSettingsClick,
}) => (
  <Grid
    item
    style={{
      width: ncols * 10,
      height: nrows * 10,
      padding: 0,
      backgroundColor: '#eee',
    }}
  >
    <Grid container direction="column" spacing={0} style={{ height: '100%', backgroundImage: `url(${backgroundImage})` }}>
      {/* settings button area */}
      <Grid item xs>
        <Grid container alignItems="flex-start" justify="flex-end">
          <Grid item>
            <Tooltip title="Settings">
              <IconButton
                color="contrast"
                onClick={onSettingsClick}
              >
                <SettingsIcon />
              </IconButton>
            </Tooltip>
          </Grid>
        </Grid>
      </Grid>
      {/* !settings button area */}
      {/* play button area */}
      <Grid item>
        <Grid container alignItems="center" justify="center" style={{ height: '100%' }}>
          <Grid item>
            <Tooltip title="Start">
              <IconButton
                color="contrast"
                style={{ width: 96, height: 96 }}
                onClick={onPlayClick}
              >
                <PlayCircleFilledIcon style={{ width: 96, height: 96 }} />
              </IconButton>
            </Tooltip>
          </Grid>
        </Grid>
      </Grid>
      {/* !play button area */}
      {/* fill the remaning area */}
      <Grid item xs />
      {/* !fill the remaning area */}
    </Grid>
  </Grid>
)
Play.propTypes = {
  backgroundImage: PropTypes.string.isRequired,
  nrows: PropTypes.number.isRequired,
  ncols: PropTypes.number.isRequired,
  onPlayClick: PropTypes.func.isRequired,
  onSettingsClick: PropTypes.func.isRequired,
}

const Pause = ({
  backgroundImage, nrows, ncols, onPauseClick,
}) => (
  <Grid
    item
    style={{
      width: ncols * 10,
      height: nrows * 10,
      padding: 0,
      backgroundColor: '#eee',
    }}
  >
    <Grid container direction="column" spacing={0} alignItems="center" justify="center" style={{ height: '100%', backgroundImage: `url(${backgroundImage})` }}>
      <Grid item>
        <Tooltip title="Pause">
          <IconButton
            color="contrast"
            style={{ width: 96, height: 96 }}
            onClick={onPauseClick}
          >
            <PauseCircleFilledIcon style={{ width: 96, height: 96 }} />
          </IconButton>
        </Tooltip>
      </Grid>
    </Grid>
  </Grid>
)
Pause.propTypes = {
  backgroundImage: PropTypes.string.isRequired,
  nrows: PropTypes.number.isRequired,
  ncols: PropTypes.number.isRequired,
  onPauseClick: PropTypes.func.isRequired,
}

const Settings = ({
  backgroundImage, classes, nrows, ncols, speed,
  underpopulation, overpopulation, safeNeighborhood, respawn,
  onBackClick, onNRowsChange, onNColsChange, onSpeedChange, onRuleChange,
}) => (
  <Grid
    item
    style={{
      width: ncols * 10,
      height: nrows * 10,
      padding: 0,
      backgroundColor: '#eee',
    }}
  >
    <Grid container direction="column" spacing={0} style={{ height: '100%', backgroundImage: `url(${backgroundImage})` }}>
      {/* back button area */}
      <Grid item xs>
        <Grid container alignItems="flex-start" justify="flex-end">
          <Grid item>
            <Tooltip title="Back home">
              <IconButton
                color="contrast"
                onClick={onBackClick}
              >
                <NavigateBeforeIcon />
              </IconButton>
            </Tooltip>
          </Grid>
        </Grid>
      </Grid>
      {/* rows */}
      <Grid item style={{ paddingLeft: 20, paddingRight: 20 }}>
        <Grid container direction="row" alignItems="center" justify="center">
          <Grid item style={{ width: 100 }}>
            <Typography style={{ color: 'white' }}>Rows</Typography>
          </Grid>
          <Grid item xs>
            <Slider
              defaultValue={nrows}
              min={25}
              step={5}
              dots
              onAfterChange={onNRowsChange}
            />
          </Grid>
        </Grid>
      </Grid>
      {/* colums */}
      <Grid item style={{ paddingLeft: 20, paddingRight: 20 }}>
        <Grid container direction="row" alignItems="center" justify="center">
          <Grid item style={{ width: 100 }}>
            <Typography style={{ color: 'white' }}>Columns</Typography>
          </Grid>
          <Grid item xs>
            <Slider
              defaultValue={ncols}
              min={40}
              step={5}
              dots
              onAfterChange={onNColsChange}
            />
          </Grid>
        </Grid>
      </Grid>
      {/* speed */}
      <Grid item style={{ paddingLeft: 20, paddingRight: 20 }}>
        <Grid container direction="row" alignItems="center" justify="center">
          <Grid item style={{ width: 100 }}>
            <Typography style={{ color: 'white' }}>Speed</Typography>
          </Grid>
          <Grid item xs>
            <Slider
              defaultValue={speed}
              min={2}
              max={20}
              step={2}
              dots
              onAfterChange={onSpeedChange}
            />
          </Grid>
        </Grid>
      </Grid>
      {/* rules */}
      <Grid item style={{ paddingLeft: 20, paddingRight: 20 }}>
        <Grid container direction="row" alignItems="flex-start" justify="center">
          <Grid item style={{ width: 100, marginTop: 10 }}>
            <Typography style={{ color: 'white' }}>Rules</Typography>
          </Grid>
          <Grid item xs>
            <FormGroup row>
              {/* underpopulation */}
              <FormControlLabel
                control={<Checkbox checked={underpopulation} onChange={onRuleChange('underpopulation')} style={{ color: 'white' }} />}
                label="Underpop."
                classes={{
                  label: classes.checkboxLabel,
                }}
              />
              {/* overpopulation */}
              <FormControlLabel
                control={<Checkbox checked={overpopulation} onChange={onRuleChange('overpopulation')} style={{ color: 'white' }} />}
                label="Overpop."
                classes={{
                  label: classes.checkboxLabel,
                }}
              />
              {/* safeNeighborhood */}
              <FormControlLabel
                control={<Checkbox checked={safeNeighborhood} onChange={onRuleChange('safeNeighborhood')} style={{ color: 'white' }} />}
                label="Safe Neighbor"
                classes={{
                  label: classes.checkboxLabel,
                }}
              />
              {/* respawn */}
              <FormControlLabel
                control={<Checkbox checked={respawn} onChange={onRuleChange('respawn')} style={{ color: 'white' }} />}
                label="Respawn"
                classes={{
                  label: classes.checkboxLabel,
                }}
              />
            </FormGroup>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs />
    </Grid>
  </Grid>
)
Settings.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
  classes: PropTypes.object.isRequired,
  backgroundImage: PropTypes.string.isRequired,
  nrows: PropTypes.number.isRequired,
  ncols: PropTypes.number.isRequired,
  speed: PropTypes.number.isRequired,
  onBackClick: PropTypes.func.isRequired,
  onNRowsChange: PropTypes.func.isRequired,
  onNColsChange: PropTypes.func.isRequired,
  onSpeedChange: PropTypes.func.isRequired,
  onRuleChange: PropTypes.func.isRequired,
  underpopulation: PropTypes.bool.isRequired,
  overpopulation: PropTypes.bool.isRequired,
  safeNeighborhood: PropTypes.bool.isRequired,
  respawn: PropTypes.bool.isRequired,
}

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  checkboxLabel: {
    color: 'white',
  },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctConwayComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    nrows: PropTypes.number,
    ncols: PropTypes.number,
    speed: PropTypes.number,
    backgroundImage: PropTypes.string.isRequired,
  }
  static defaultProps = {
    nrows: 25,
    ncols: 40,
    speed: 10,
  }

  componentWillMount() {
    debug('componentWillMount()')
    const { nrows, ncols, speed } = this.props
    this.nrows = nrows
    this.ncols = ncols
    this.speed = speed
    this.reset()
  }
  componentWillUnmount() {
    debug('componentWillUnmount()')
    if (this.intervalId) {
      clearInterval(this.intervalId)
    }
  }

  getNeighborhood({ newBoard, x, y }) {
    // debug('getNeighborhood()')
    const nw = y > 0 && x > 0 && newBoard[y - 1][x - 1]
    const n = y > 0 && newBoard[y - 1][x]
    const ne = y > 0 && x < this.ncols - 1 && newBoard[y - 1][x + 1]
    const w = x < this.ncols - 1 && newBoard[y][x - 1]
    const o = newBoard[y][x]
    const e = x < this.ncols - 1 && newBoard[y][x + 1]
    const sw = y < this.nrows - 1 && x > 0 && newBoard[y + 1][x - 1]
    const s = y < this.nrows - 1 && newBoard[y + 1][x]
    const se = y < this.nrows - 1 && x < this.ncols - 1 && newBoard[y + 1][x + 1]
    return {
      nw, n, ne, w, o, e, sw, s, se,
    }
  }
  countLiveNeighbors({ newBoard, x, y }) {
    // debug('countLiveNeighbors()')
    const {
      nw, n, ne,
      w, e,
      sw, s, se,
    } = this.getNeighborhood({ newBoard, y, x })
    return nw + n + ne + w + e + sw + s + se
  }

  reset() {
    debug('reset()')
    this.board = (
      Array(this.nrows).fill().map(() => (
        Array(this.ncols).fill().map(() => (
          !!Math.round(Math.random())
        ))
      ))
    )
    debug(this.board.slice().map(x => x.slice()))
  }

  @observable nrows
  @observable ncols
  @observable board = []
  @observable isPlaying = false
  @observable isHovered = false
  @observable inSettings = false
  @observable rules = {
    underpopulation: true,
    overpopulation: true,
    safeNeighborhood: true,
    respawn: true,
  }
  @observable speed

  intervalId = undefined
  counter = 0

  @action handlePlayClick = () => {
    debug('handlePlayClick()')
    this.isPlaying = true
    this.intervalId = setInterval(action(() => {
      this.counter += 1
      debug(`counter: ${this.counter}`)
      // copy observable
      const newBoard = this.board.slice().map(x => x.slice())
      // loop on rows
      for (let y = 0; y < this.nrows; y += 1) {
        // loop on cols
        for (let x = 0; x < this.ncols; x += 1) {
          // rule 1
          const sum = this.countLiveNeighbors({ newBoard, y, x })
          let candidate = newBoard[y][x]
          // rule 1
          if (this.rules.underpopulation && newBoard[y][x] && sum < 2) {
            candidate = false
          }
          // rule 2
          if (this.rules.overpopulation && newBoard[y][x] && sum > 3) {
            candidate = false
          }
          // rule 3
          if (this.rules.safeNeighborhood && newBoard[y][x] && (sum === 2 || sum === 3)) {
            candidate = newBoard[y][x]
          }
          // rule 4
          if (this.rules.respawn && !newBoard[y][x] && sum === 3) {
            candidate = true
          }
          // apply new value
          newBoard[y][x] = candidate
        }
      }
      // override observable
      this.board = newBoard
      // TBD
    }), (22 - this.speed) * 100)
  }
  @action handlePauseClick = () => {
    debug('handlePauseClick()')
    this.isPlaying = false
    clearInterval(this.intervalId)
  }
  @action handleBoardMouseEnter = () => {
    debug('handleBoardMouseEnter()')
    this.isHovered = true
  }
  @action handleBoardMouseLeave = () => {
    debug('handleBoardMouseLeave()')
    this.isHovered = false
  }
  @action handleSettingsClick = () => {
    debug('handleSettingsClick()')
    this.inSettings = true
  }
  @action handleBackClick = () => {
    debug('handleBackClick()')
    this.inSettings = false
  }
  @action handleNRowsChange = (value) => {
    debug('handleNRowsChange()')
    debug(value)
    this.nrows = value
    this.reset()
  }
  @action handleNColsChange = (value) => {
    debug('handleNColsChange()')
    debug(value)
    this.ncols = value
    this.reset()
  }
  @action handleSpeedChange = (value) => {
    debug('handleSpeedChange()')
    debug(value)
    this.speed = value
    this.reset()
  }
  handleRuleChange = rule => action((event, checked) => {
    debug('handleRuleChange()')
    this.rules[rule] = checked
  })

  render() {
    debug('render()')
    const { classes, backgroundImage } = this.props
    return (
      <Grid
        container
        direction="column"
        alignItems="center"
        justify="center"
        style={{ height: '90vh' }}
      >
        <Grid item>
          <Grid
            container
            direction="column"
            onMouseEnter={this.handleBoardMouseEnter}
            onMouseLeave={this.handleBoardMouseLeave}
          >
            {/* board */}
            { this.isPlaying && !this.isHovered && (
              <Board
                nrows={this.nrows}
                ncols={this.ncols}
                board={this.board.slice().map(x => x.slice())}
              />
            ) }
            {/* play */}
            { !this.isPlaying && !this.inSettings && (
              <Play
                backgroundImage={backgroundImage}
                nrows={this.nrows}
                ncols={this.ncols}
                onPlayClick={this.handlePlayClick}
                onSettingsClick={this.handleSettingsClick}
              />
            ) }
            {/* pause */}
            { this.isPlaying && this.isHovered && (
              <Pause
                backgroundImage={backgroundImage}
                nrows={this.nrows}
                ncols={this.ncols}
                onPauseClick={this.handlePauseClick}
              />
            ) }
            {/* settings */}
            { !this.isPlaying && this.inSettings && (
              <Settings
                backgroundImage={backgroundImage}
                nrows={this.nrows}
                ncols={this.ncols}
                speed={this.speed}
                underpopulation={this.rules.underpopulation}
                overpopulation={this.rules.overpopulation}
                safeNeighborhood={this.rules.safeNeighborhood}
                respawn={this.rules.respawn}
                classes={{
                  checkboxLabel: classes.checkboxLabel,
                }}
                onBackClick={this.handleBackClick}
                onNRowsChange={this.handleNRowsChange}
                onNColsChange={this.handleNColsChange}
                onSpeedChange={this.handleSpeedChange}
                onRuleChange={this.handleRuleChange}
              />
            ) }
          </Grid>
        </Grid>
      </Grid>
    )
  }
}
