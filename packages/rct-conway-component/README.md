# @4geit/rct-conway-component [![npm version](//badge.fury.io/js/@4geit%2Frct-conway-component.svg)](//badge.fury.io/js/@4geit%2Frct-conway-component)

---

conway game engine component

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-conway-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-conway-component) package manager using the following command:

```bash
npm i @4geit/rct-conway-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-conway-component
```

2. Depending on where you want to use the component you will need to import the class `RctConwayComponent` to your project JS file as follows:

```js
import RctConwayComponent from '@4geit/rct-conway-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctConwayComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctConwayComponent from '@4geit/rct-conway-component'
// ...
const App = () => (
  <div className="App">
    <RctConwayComponent/>
  </div>
)
```
