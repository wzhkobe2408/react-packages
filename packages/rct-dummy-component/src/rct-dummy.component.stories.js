import React from 'react'
import { storiesOf } from '@storybook/react'
import centered from '@storybook/addon-centered'
import { withInfo } from '@storybook/addon-info'

import RctDummyComponent from './rct-dummy.component'

storiesOf('RctDummyComponent', module)
  .addDecorator(centered)
  .add('simple usage', withInfo()(() => (
    <RctDummyComponent />
  )))
