import React from 'react'
import ReactDOM from 'react-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import RctDummyComponent, { ListComponent, ItemComponent } from './rct-dummy.component'

const debug = buildDebug('react-packages:packages:test:rct-dummy-component')

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctDummyComponent$/,
  })
}

let mount
let shallow

beforeEach(() => {
  debug('beforeEach')
  mount = createMount()
  shallow = createShallow({ untilSelector: 'div' })
})
afterEach(() => {
  debug('afterEach')
  mount.cleanUp()
})

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <RctDummyComponent />,
    div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount(<RctDummyComponent />)
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctDummyComponent />)
  expect(wrapper).toMatchSnapshot()
})

describe('list component', () => {
  debug('list component')
  test('display 10 items', () => {
    debug('display 10 items')
    const wrapper = mount(<RctDummyComponent />)
    expect(wrapper.find(ListComponent).find(ItemComponent)).toHaveLength(10)
  })
})
