import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import notificationStore from '@4geit/rct-notification-store'

import RctHeaderComponent from '@4geit/rct-header-component'
import RctRightSideMenuComponent from '@4geit/rct-right-side-menu-component'
import RctSideMenuComponent, { SideMenuItem } from '@4geit/rct-side-menu-component'
import RctLayoutComponent from './rct-layout.component'
import Logo from './assets/logo.png'

const debug = buildDebug('react-packages:packages:test:rct-layout-component')

const stores = {
  commonStore,
  notificationStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctLayoutComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores}>
      <BrowserRouter>
        <RctLayoutComponent
          topComponent={<RctHeaderComponent logo={Logo} />}
          sideMenuComponent={
            <RctSideMenuComponent >
              <SideMenuItem icon="inbox" label="Inbox" route="/" />
              <SideMenuItem icon="star" label="Starred" route="/" />
              <SideMenuItem icon="send" label="Send mail" route="/" />
              <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
              <SideMenuItem icon="email" label="All mail" route="/" />
              <SideMenuItem icon="delete" label="Trash" route="/" />
              <SideMenuItem icon="spam" label="Spam" route="/" />
            </RctSideMenuComponent>
          }
          rightSideMenuComponent={
            <RctRightSideMenuComponent>
              <div>right side menu</div>
            </RctRightSideMenuComponent>
          }
        >
          <div>MY CONTENT</div>
        </RctLayoutComponent>
      </BrowserRouter>
    </Provider>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <Provider {...stores}>
//       <BrowserRouter>
//         <RctLayoutComponent
//           topComponent={<RctHeaderComponent logo={Logo} />}
//           sideMenuComponent={
//             <RctSideMenuComponent >
//               <SideMenuItem icon="inbox" label="Inbox" route="/" />
//               <SideMenuItem icon="star" label="Starred" route="/" />
//               <SideMenuItem icon="send" label="Send mail" route="/" />
//               <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
//               <SideMenuItem icon="email" label="All mail" route="/" />
//               <SideMenuItem icon="delete" label="Trash" route="/" />
//               <SideMenuItem icon="spam" label="Spam" route="/" />
//             </RctSideMenuComponent>
//             }
//           rightSideMenuComponent={
//             <RctRightSideMenuComponent>
//               <div>right side menu</div>
//             </RctRightSideMenuComponent>
//             }
//         >
//           <div>MY CONTENT</div>
//         </RctLayoutComponent>
//       </BrowserRouter>
//     </Provider>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <RctLayoutComponent
      {...stores}
      topComponent={<RctHeaderComponent logo={Logo} />}
      sideMenuComponent={
        <RctSideMenuComponent >
          <SideMenuItem icon="inbox" label="Inbox" route="/" />
          <SideMenuItem icon="star" label="Starred" route="/" />
          <SideMenuItem icon="send" label="Send mail" route="/" />
          <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
          <SideMenuItem icon="email" label="All mail" route="/" />
          <SideMenuItem icon="delete" label="Trash" route="/" />
          <SideMenuItem icon="spam" label="Spam" route="/" />
        </RctSideMenuComponent>
      }
      rightSideMenuComponent={
        <RctRightSideMenuComponent>
          <div>right side menu</div>
        </RctRightSideMenuComponent>
      }
    >
      <div>MY CONTENT</div>
    </RctLayoutComponent>
  ))
  expect(wrapper).toMatchSnapshot()
})
