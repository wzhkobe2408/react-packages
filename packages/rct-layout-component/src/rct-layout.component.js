import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'

import classNames from 'classnames'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import RctNotificationComponent from '@4geit/rct-notification-component'

import './rct-layout.component.css'

const debug = buildDebug('react-packages:packages:rct-layout-component')

@withStyles(theme => ({
  fill: {
    height: '100%',
  },
  responsive: {
    [theme.breakpoints.down('lg')]: {
      width: '900px',
    },
    [theme.breakpoints.up('lg')]: {
      width: '1200px',
    },
    margin: 'auto',
    padding: '0 20px',
  },
  header: {
    minHeight: '50px',
  },
  middleTop: {
    paddingTop: '90px',
    backgroundColor: '#fff',
  },
  middleBottom: {
    padding: '10px',
    backgroundColor: '#fff',
    borderTop: '1px solid #eee',
  },
  footer: {
    borderTop: '1px solid #eee',
    backgroundColor: '#fff',
    height: '50px',
  },
  sideMenu: {
  },
  content: {
    borderLeft: '1px solid #eee',
    padding: '10px',
  },
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctLayoutComponent extends Component {
  static propTypes = {
    /* eslint-disable react/forbid-prop-types */
    /* eslint-disable react/no-unused-prop-types */
    classes: PropTypes.any.isRequired,
    width: PropTypes.string.isRequired,
    /* eslint-enable react/no-unused-prop-types */
    topComponent: PropTypes.object.isRequired,
    sideMenuComponent: PropTypes.any.isRequired,
    rightSideMenuComponent: PropTypes.any,
    footerComponent: PropTypes.any,
    notificationComponent: PropTypes.any,
    children: PropTypes.any.isRequired,
    /* eslint-enable react/forbid-prop-types */
  }
  static defaultProps = {
    notificationComponent: <RctNotificationComponent />,
    rightSideMenuComponent: undefined,
    footerComponent: undefined,
  }

  render() {
    debug('render()')
    const {
      children, classes, topComponent,
      footerComponent, notificationComponent,
      sideMenuComponent: _sideMenuComponent,
      rightSideMenuComponent: _rightSideMenuComponent,
    } = this.props
    const rightSideMenuComponent = _rightSideMenuComponent
      && React.cloneElement(_rightSideMenuComponent, {
        contentComponent: children,
      })
    const sideMenuComponent = _sideMenuComponent
      && React.cloneElement(_sideMenuComponent, {
        topComponent,
        contentComponent: rightSideMenuComponent || children,
      })
    return (
      <div className={classes.fill} >
        {/* root */}
        <div className={classes.fill} >
          {/* middle */}
          <div>
            {/* side menu */}
            { sideMenuComponent && (
              <div className={classes.sideMenu} >
                { sideMenuComponent }
              </div>
            ) }
            {/* !side menu */}
          </div>
          {/* !middle */}
          {/* footer */}
          { footerComponent && (
            <div className={classNames(classes.responsive, classes.footer)} >
              { footerComponent }
            </div>
          ) }
          {/* !footer */}
          {/* notification */}
          { notificationComponent }
          {/* !notification */}
        </div>
        {/* !root */}
      </div>
    )
  }
}
