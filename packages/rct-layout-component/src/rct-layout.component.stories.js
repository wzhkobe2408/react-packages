import React from 'react'
import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import List, { ListItem, ListItemText } from 'material-ui/List'

import commonStore from '@4geit/rct-common-store'
import notificationStore from '@4geit/rct-notification-store'

import RctHeaderComponent from '@4geit/rct-header-component'
import RctRightSideMenuComponent from '@4geit/rct-right-side-menu-component'
import RctSideMenuComponent, { SideMenuItem } from '@4geit/rct-side-menu-component'
import RctLayoutComponent from './rct-layout.component'
import Logo from './assets/logo.png'

const stores = {
  commonStore,
  notificationStore,
}

storiesOf('RctLayoutComponent', module)
  .add('with header, side-menu and content', withInfo()(() => (
    <Provider {...stores}>
      <BrowserRouter>
        <RctLayoutComponent
          topComponent={<RctHeaderComponent logo={Logo} />}
          sideMenuComponent={
            <RctSideMenuComponent >
              <SideMenuItem icon="inbox" label="Inbox" route="/" />
              <SideMenuItem icon="star" label="Starred" route="/" />
              <SideMenuItem icon="send" label="Send mail" route="/" />
              <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
              <SideMenuItem icon="email" label="All mail" route="/" />
              <SideMenuItem icon="delete" label="Trash" route="/" />
              <SideMenuItem icon="spam" label="Spam" route="/" />
            </RctSideMenuComponent>
          }
          rightSideMenuComponent={
            <RctRightSideMenuComponent>
              <List>
                <ListItem button>
                  <ListItemText primary="chatbox1" />
                </ListItem>
                <ListItem button>
                  <ListItemText primary="chatbox2" />
                </ListItem>
                <ListItem button>
                  <ListItemText primary="chatbox3" />
                </ListItem>
                <ListItem button>
                  <ListItemText primary="chatbox4" />
                </ListItem>
              </List>
            </RctRightSideMenuComponent>
          }
        >
          <div>MY CONTENT</div>
        </RctLayoutComponent>
      </BrowserRouter>
    </Provider>
  )))
