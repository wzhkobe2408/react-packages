# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.98.0"></a>
# [1.98.0](https://gitlab.com/4geit/react-packages/compare/v1.97.4...v1.98.0) (2017-12-10)


### Features

* **book sample:** changing images ([2ba15b2](https://gitlab.com/4geit/react-packages/commit/2ba15b2))
* **book sample component:** image and article selection ([a1df990](https://gitlab.com/4geit/react-packages/commit/a1df990))




<a name="1.97.4"></a>
## [1.97.4](https://gitlab.com/4geit/react-packages/compare/v1.97.3...v1.97.4) (2017-12-08)


### Bug Fixes

* **landing-page-layout:** fix layout ([03e3d1c](https://gitlab.com/4geit/react-packages/commit/03e3d1c))




<a name="1.92.1"></a>
## [1.92.1](https://gitlab.com/4geit/react-packages/compare/v1.92.0...v1.92.1) (2017-12-02)


### Bug Fixes

* **book-sample:** minor ([e752e25](https://gitlab.com/4geit/react-packages/commit/e752e25))




<a name="1.89.1"></a>
## [1.89.1](https://gitlab.com/4geit/react-packages/compare/v1.89.0...v1.89.1) (2017-11-29)




**Note:** Version bump only for package @4geit/rct-book-sample-component

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)


### Bug Fixes

* **tests:** update snapshots ([df2c84b](https://gitlab.com/4geit/react-packages/commit/df2c84b))




<a name="1.87.3"></a>
## [1.87.3](https://gitlab.com/4geit/react-packages/compare/v1.87.2...v1.87.3) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-book-sample-component

<a name="1.86.3"></a>
## [1.86.3](https://gitlab.com/4geit/react-packages/compare/v1.86.2...v1.86.3) (2017-11-21)




**Note:** Version bump only for package @4geit/rct-book-sample-component

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-book-sample-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)


### Features

* **book sample:** created package ([18619c3](https://gitlab.com/4geit/react-packages/commit/18619c3))
