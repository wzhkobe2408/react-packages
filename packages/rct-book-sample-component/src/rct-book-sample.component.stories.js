import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import centered from '@storybook/addon-centered'
import { withInfo } from '@storybook/addon-info'
import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
// import xyzStore from '@4geit/rct-xyz-store'
import sampleImage1 from './sampleImage1.jpg'
import sampleImage2 from './sampleImage2.jpg'
import sampleImage3 from './sampleImage3.jpg'
import sampleImage4 from './sampleImage4.jpg'
import sampleArticle1 from './sampleArticle1.jpg'
import sampleArticle2 from './sampleArticle2.jpg'
import sampleArticle3 from './sampleArticle3.jpg'
import RctBookSampleComponent, { BookSample } from './rct-book-sample.component'

const stores = {
  commonStore,
  swaggerClientStore,
  // xyzStore,
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

storiesOf('RctBookSampleComponent', module)
  .addDecorator(centered)
  .add('simple usage', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores}>
        <App>
          <RctBookSampleComponent>
            <BookSample image={sampleImage1} article={sampleArticle1} />
            <BookSample image={sampleImage2} article={sampleArticle2} />
            <BookSample image={sampleImage3} article={sampleArticle3} />
            <BookSample image={sampleImage4} article={sampleArticle3} />
          </RctBookSampleComponent>
        </App>
      </Provider>
    </BrowserRouter>
  )))
