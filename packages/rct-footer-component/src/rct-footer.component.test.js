import React from 'react'
import ReactDOM from 'react-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import RctFooterComponent, { FooterItem } from './rct-footer.component'

const debug = buildDebug('react-packages:packages:test:rct-footer-component')

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctFooterComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <RctFooterComponent>
      <FooterItem icon="restore" label="Recents" />
      <FooterItem icon="favorite" label="Favorites" />
    </RctFooterComponent>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <RctFooterComponent>
//       <FooterItem icon="restore" label="Recents" />
//       <FooterItem icon="favorite" label="Favorites" />
//     </RctFooterComponent>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <RctFooterComponent>
      <FooterItem icon="restore" label="Recents" />
      <FooterItem icon="favorite" label="Favorites" />
    </RctFooterComponent>
  ))
  expect(wrapper).toMatchSnapshot()
})
