# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.81.2"></a>
## [1.81.2](https://gitlab.com/4geit/react-packages/compare/v1.81.1...v1.81.2) (2017-10-31)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-footer-component

<a name="1.23.0"></a>
# [1.23.0](https://gitlab.com/4geit/react-packages/compare/v1.21.0...v1.23.0) (2017-09-08)


### Features

* **sidemenu/footer:** dynamic item icons ([494e731](https://gitlab.com/4geit/react-packages/commit/494e731))




<a name="1.22.0"></a>
# [1.22.0](https://gitlab.com/4geit/react-packages/compare/v1.21.0...v1.22.0) (2017-09-08)


### Features

* **sidemenu/footer:** dynamic item icons ([494e731](https://gitlab.com/4geit/react-packages/commit/494e731))




<a name="1.16.1"></a>
## [1.16.1](https://gitlab.com/4geit/react-packages/compare/v1.16.0...v1.16.1) (2017-09-07)


### Bug Fixes

* **footer item:** use of the BottomNqvigqtion Button to make it dynamic ([29e32af](https://gitlab.com/4geit/react-packages/commit/29e32af))




<a name="1.16.0"></a>
# [1.16.0](https://gitlab.com/4geit/react-packages/compare/v1.15.4...v1.16.0) (2017-09-06)


### Features

* **footer:** customize list item ([7707da1](https://gitlab.com/4geit/react-packages/commit/7707da1))


### Performance Improvements

* **footer item:** fixed issue ([b077b87](https://gitlab.com/4geit/react-packages/commit/b077b87))




<a name="1.15.2"></a>
## [1.15.2](https://gitlab.com/4geit/react-packages/compare/v1.15.0...v1.15.2) (2017-09-05)


### Bug Fixes

* **footer:** fix version number ([a678909](https://gitlab.com/4geit/react-packages/commit/a678909))


### Features

* **footer:** added footer app ([33e8ead](https://gitlab.com/4geit/react-packages/commit/33e8ead))
