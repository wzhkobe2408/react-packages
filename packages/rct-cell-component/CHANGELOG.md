# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.97.0"></a>
# [1.97.0](https://gitlab.com/4geit/react-packages/compare/v1.96.0...v1.97.0) (2017-12-07)


### Features

* **conway component:** completions ([44f4d97](https://gitlab.com/4geit/react-packages/commit/44f4d97))




<a name="1.96.0"></a>
# [1.96.0](https://gitlab.com/4geit/react-packages/compare/v1.95.0...v1.96.0) (2017-12-06)


### Features

* **componens:** add new componets as part of the conways game implementation game ([2f19544](https://gitlab.com/4geit/react-packages/commit/2f19544))
