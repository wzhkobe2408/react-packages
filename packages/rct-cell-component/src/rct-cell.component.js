import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line no-unused-vars
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import './rct-cell.component.css'

const debug = buildDebug('react-packages:packages:rct-cell-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctCellComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    row: PropTypes.number.isRequired,
    col: PropTypes.number.isRequired,
    filled: PropTypes.bool,
    // TBD
  }
  static defaultProps = {
    filled: false,
    // TBD
  }

  render() {
    // debug('render()')
    const { row, col, filled } = this.props
    return (
      <rect
        width={10}
        height={10}
        y={row * 10}
        x={col * 10}
        fill={filled ? 'black' : 'white'}
        stroke="black"
        strokeWidth={1}
      />
    )
  }
}
