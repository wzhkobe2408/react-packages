import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { inject, observer } from 'mobx-react'

import Typography from 'material-ui/Typography'
import IconButton from 'material-ui/IconButton'
import AccountBoxIcon from 'material-ui-icons/AccountBox'
import PowerSettingsNewIcon from 'material-ui-icons/PowerSettingsNew'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'
import Tooltip from 'material-ui/Tooltip'

import { RctNotificationStore } from '@4geit/rct-notification-store'

import RctSearchInputComponent, { SearchInputSource } from '@4geit/rct-search-input-component'

import './rct-header.component.css'

const debug = buildDebug('react-packages:packages:rct-header-component')

@withStyles(theme => ({
  flex: {
    flex: 1,
  },
  button: {
    margin: theme.spacing.unit,
  },
  gridContainer: {
    marginRight: 'auto',
  },
  logoStyle: {
    height: 50,
  },
  names: {
    color: 'inherit',
  },
}))
@withWidth()
@inject('commonStore', 'notificationStore')
@withRouter
@observer
export default class RctHeaderComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    commonStore: PropTypes.any.isRequired,
    notificationStore: PropTypes.instanceOf(RctNotificationStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    history: PropTypes.object.isRequired,
    logo: PropTypes.string,
    accountRoute: PropTypes.string,
    loginRoute: PropTypes.string,
    logoutRedirectTo: PropTypes.string,
    // eslint-disable-next-line react/forbid-prop-types
    searchInputComponent: PropTypes.any,
  }
  static defaultProps = {
    logo: undefined,
    accountRoute: '/account',
    loginRoute: '/login',
    logoutRedirectTo: '/login',
    searchInputComponent: undefined,
  }

  handleLogout = () => {
    debug('handleLogout()')
    this.props.commonStore.logout()
    this.props.notificationStore.newMessage('You are logged out!')
    this.props.history.replace(this.props.logoutRedirectTo)
  }

  render() {
    debug('render()')
    const {
      classes, logo, accountRoute, loginRoute, searchInputComponent,
    } = this.props
    const { isLoggedIn, firstname, lastname } = this.props.commonStore
    const { gridContainer, logoStyle, names } = classes
    return (
      // we need a layout grid component to arrange the position
      // of the items such as the logo, the search bar as well as
      // the action buttons
      <Grid container alignItems="center" className={gridContainer} >
        {/* logo */}
        <Grid item xs>
          <Link to="/">
            <img src={logo} alt="Logo" className={logoStyle} />
          </Link>
        </Grid>
        {/* !logo */}
        {/* searchbar */}
        <Grid item>
          { isLoggedIn && !searchInputComponent &&
            <RctSearchInputComponent>
              <SearchInputSource listOperationId="productList" field="name" />
              <SearchInputSource listOperationId="contactList" field="firstname" />
            </RctSearchInputComponent>
          }
          { isLoggedIn && searchInputComponent }
        </Grid>
        {/* !searchbar */}
        {/* actions */}
        <Grid item>
          { !isLoggedIn && (
            <Tooltip title="Sign in">
              <IconButton component={Link} to={loginRoute} color="contrast">
                <AccountBoxIcon />
              </IconButton>
            </Tooltip>
          ) }
          { isLoggedIn && (
            <Tooltip title="Account">
              <IconButton component={Link} to={accountRoute} color="contrast">
                <AccountBoxIcon />
              </IconButton>
            </Tooltip>
          ) }
          { isLoggedIn && (
            <Tooltip title="Sign out">
              <IconButton aria-label="Logout" color="contrast" onClick={this.handleLogout} >
                <PowerSettingsNewIcon />
              </IconButton>
            </Tooltip>
          ) }
        </Grid>
        <Grid item>
          <Typography type="body1" className={names} >
            { firstname } { lastname }
          </Typography>
        </Grid>
        {/* !actions */}
      </Grid>
    )
  }
}
