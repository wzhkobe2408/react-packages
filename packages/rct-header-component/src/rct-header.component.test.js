import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import notificationStore from '@4geit/rct-notification-store'

import RctHeaderComponent from './rct-header.component'

const debug = buildDebug('react-packages:packages:test:rct-header-component')

const stores = {
  commonStore,
  notificationStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctHeaderComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <BrowserRouter>
      <Provider {...stores} >
        <RctHeaderComponent />
      </Provider>
    </BrowserRouter>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <BrowserRouter>
//       <RctHeaderComponent {...stores} />
//     </BrowserRouter>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <BrowserRouter>
      <RctHeaderComponent {...stores} />
    </BrowserRouter>
  ))
  expect(wrapper).toMatchSnapshot()
})
