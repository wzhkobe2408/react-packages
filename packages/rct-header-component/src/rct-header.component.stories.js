import React, { Component } from 'react'
import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { withStyles } from 'material-ui/styles'

import commonStore from '@4geit/rct-common-store'
import notificationStore from '@4geit/rct-notification-store'

import RctLayoutComponent from '@4geit/rct-layout-component'
import RctSideMenuComponent from '@4geit/rct-side-menu-component'
import RctHeaderComponent from './rct-header.component'
import Logo from './assets/logo.png'

const stores = {
  commonStore,
  notificationStore,
}

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  gridContainer: {
    marginLeft: 100,
  },
  logoStyle: {
    backgroundColor: 'white',
    padding: 2,
  },
}))
class RctHeaderComponentWrapper extends Component {
  render() {
    return (
      <RctHeaderComponent {...this.props} />
    )
  }
}

storiesOf('RctHeaderComponent', module)
  .add('simple usage', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores} >
        <RctHeaderComponent logo={Logo} />
      </Provider>
    </BrowserRouter>
  )))
  .add('with layout and side-menu', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores} >
        <RctLayoutComponent
          topComponent={
            <RctHeaderComponent logo={Logo} />
          }
          sideMenuComponent={
            <RctSideMenuComponent />
          }
        />
      </Provider>
    </BrowserRouter>
  )))
  .add('with layout and side-menu disabled', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores} >
        <RctLayoutComponent
          topComponent={
            <RctHeaderComponent logo={Logo} />
          }
          sideMenuComponent={
            <RctSideMenuComponent enabled={false} />
          }
        />
      </Provider>
    </BrowserRouter>
  )))
  .add('customized header', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores} >
        <RctLayoutComponent
          topComponent={
            <RctHeaderComponentWrapper logo={Logo} />
          }
          sideMenuComponent={
            <RctSideMenuComponent enabled={false} />
          }
        />
      </Provider>
    </BrowserRouter>
  )))
