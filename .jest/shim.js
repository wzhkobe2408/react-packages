// fix babel-polyfix issue
global.requestAnimationFrame = function(callback) {
  setTimeout(callback, 0)
}

// add the CSS.escape utility method
require('css.escape')
